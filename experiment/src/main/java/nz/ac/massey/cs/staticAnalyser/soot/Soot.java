package nz.ac.massey.cs.staticAnalyser.soot;

import probe.CallEdge;
import probe.GXLWriter;
import probe.ObjectManager;
import probe.ProbeMethod;
import soot.*;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Targets;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Soot call graph construction
 * @author li sui
 */
public class Soot {

    private probe.CallGraph probeCG;

    public void run(String processDir,boolean reflectionAnalysis){
        List<String> argsList = new ArrayList<>();
        argsList.addAll(Arrays.asList("-d","sootOutput",
                "-whole-program",
                "-p","cg","all-reachable",
                "-p","cg","implicit-entry",
                "-p","cg.spark","enabled",
                "-p","cg.spark","vta"
        ));
        //reflection analysis enabled
        if(reflectionAnalysis){
           argsList.addAll(Arrays.asList(
                   "-p","cg","safe-forname",//produce large FP
                   "-p","cg","safe-newinstance"//produce large FP
                   //"-p","cg","types-for-invoke"//this option does not work: java.lang.ClassCastException: soot.AnySubType cannot be cast to soot.RefType
           ));
        }
        argsList.addAll(Arrays.asList(
                "-allow-phantom-refs",
                "-f","n",
                "-process-dir",
                processDir
        ));

        PackManager.v().getPack("wjtp").add(new Transform("wjtp.my", new SceneTransformer() {
            @Override
            protected void internalTransform(String phaseName, Map options) {
                //CHATransformer.v().transform();
                CallGraph cg = Scene.v().getCallGraph();
                try {
                    probeCG =build(cg);
                } catch (Exception e) {}
            }
        }));
        soot.Main.main(argsList.toArray(new String[0]));
    }

    private probe.CallGraph build(CallGraph sootCG ) throws Exception{
        probe.CallGraph cg=new probe.CallGraph();
        Iterator<MethodOrMethodContext> methods = sootCG.sourceMethods();
        while (methods.hasNext()) {
            SootMethod source = (SootMethod)methods.next();
            Iterator<MethodOrMethodContext> targets = new Targets(sootCG.edgesOutOf(source));
            while (targets.hasNext()) {
                SootMethod target = (SootMethod)targets.next();
//                if(!source.getDeclaringClass().toString().contains("jdk.internal.org")
//                        && !target.getDeclaringClass().toString().contains("jdk.internal.org") && !source.isJavaLibraryMethod() && !target.isJavaLibraryMethod()){
                    ProbeMethod probeSource = ObjectManager.v().getMethod(ObjectManager.v()
                            .getClass(source.getDeclaringClass().toString()),source.getName(),buildMethodSignature(source));
                    ProbeMethod probeTarget = ObjectManager.v().getMethod(ObjectManager.v()
                            .getClass(target.getDeclaringClass().toString()),target.getName(),buildMethodSignature(target));
                    probe.CallEdge probeEdge = new CallEdge(probeSource,probeTarget) ;
                    cg.entryPoints().add(probeSource);
                    ((Set<CallEdge>)cg.edges()).add(probeEdge);
                //}
            }
        }

//        GXLWriter gxlwriter = new GXLWriter();
//        String fileName = new SimpleDateFormat("yyyyMMddhhmmsssSSSSSSS'.xml'").format(new Date());
//        gxlwriter.write(cg, new FileOutputStream(fileName, false));
        return cg;
    }

    private String buildMethodSignature(SootMethod method){
        StringBuilder builder=new StringBuilder();
        builder.append(method.getReturnType().toString()+" "+method.getName()+"(");

        for (int i = 0; i < method.getParameterTypes().size(); i++) {
            builder.append(method.getParameterTypes().get(i));
            if(i<method.getParameterTypes().size()-1){
                builder.append(",");
            }
        }
        builder.append(")");
        return builder.toString();
    }

    public probe.CallGraph getProbeCG(){
        return this.probeCG;
    }
}
