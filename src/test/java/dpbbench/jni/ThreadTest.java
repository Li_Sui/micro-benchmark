package dpbbench.jni;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.PositiveTests;
import dpbbench.jni.Thread;

public class ThreadTest {
	private Thread thread;

	@Before
	public void setUp() throws Exception {
		thread = new Thread();
		thread.source();
	}

	@After
	public void tearDown() throws Exception {
		thread = null;
	}

	/**
	 * test if Thread.source() does call Thread.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTarget() throws Exception {
		java.lang.Thread.sleep(4000);// sleep for 4 seconds to make sure the
										// target is called
		Assert.assertTrue(thread.TARGET);
	}
}