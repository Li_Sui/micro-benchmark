package dpbbench.unsafe;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Utility;

/**
 * Change the field reference via the unsafe API. UnsafeTypeConfusion.source()
 * is suppose to call UnsafeTypeConfusion$Target.target(), but the reference
 * value changed to UnsafeTypeConfusion$Target2
 *
 * @author Li Sui
 */
public class UnsafeTypeConfusion {

	public boolean TARGET = false;
	public boolean TARGET2 = false;
	public Target target;

	@Source
	public void source() throws Exception {
		target = new Target();
		Utility.getUnsafe().putObject(this,
				Utility.getUnsafe().objectFieldOffset(UnsafeTypeConfusion.class.getDeclaredField("target")),
				new Target2());
		target.target();
	}

	public class Target {
		@dpbbench.util.Target(expectation = NO)
		public void target() {
			TARGET = true;
		}
	}

	public class Target2 {
		@dpbbench.util.Target(expectation = YES)
		public void target() {
			TARGET2 = true;
		}
	}
}
