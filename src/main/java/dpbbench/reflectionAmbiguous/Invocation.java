package dpbbench.reflectionAmbiguous;

import static dpbbench.util.Expected.MAYBE;
import static dpbbench.util.Expected.NO;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Interprocedural usage: method is supplied by searching for the annotated
 * method. The first method found returns. Reference to a pattern found in Log4j:
 * org.apache.logging.log4j.core.config.plugins.util.PluginBuilder.build->findFactoryMethod
 *
 * The analyser reports accurate results w.r.t. the observable edges for some,
 * but not all, tested JVMs. They are Oracle JRE 1.8.0 144, Openjdk JRE 1.8.0 40 and IBM JRE 1.8.0 171.
 * 
 * @author Li Sui
 */
public class Invocation {

	public boolean TARGET = false;
	public boolean TARGET2 = false;
	public boolean TARGET3 = false;

	@Source
	public void source() throws Exception {
		for (java.lang.reflect.Method method : Invocation.class.getDeclaredMethods()) {
			if (method.isAnnotationPresent(Method.class)) {
				method.invoke(this, null);
				return;
			}
		}
	}

	@Method
	@Target(expectation = MAYBE)
	public void target() {
		this.TARGET = true;
	}

	@Method
	@Target(expectation = MAYBE)
	public void target2() {
		this.TARGET2 = true;
	}

	@Target(expectation = NO)
	public void target3() {
		this.TARGET3 = true;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@java.lang.annotation.Target(ElementType.METHOD)
	public @interface Method {
	}
}
