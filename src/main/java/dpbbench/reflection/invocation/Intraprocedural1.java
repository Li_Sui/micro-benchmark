package dpbbench.reflection.invocation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;

import dpbbench.util.Source;
import dpbbench.util.Target;
import java.lang.reflect.Method;

/**
 * Intraprocedural usage: method name is provided via a series of
 * transformations.
 *
 * @author Li Sui
 */
public class Intraprocedural1 {

	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		String methodName = new StringBuilder("TEGRAT").reverse().toString().toLowerCase();
		Method m = Intraprocedural1.class.getDeclaredMethod(methodName, null);
		m.invoke(this, null);
	}

	@Target(expectation = NO)
	public void TEGRAT() {
		this.TARGET2 = true;
	}

	@Target(expectation = YES)
	public void target() {
		this.TARGET = true;
	}
}
