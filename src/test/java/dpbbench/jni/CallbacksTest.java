package dpbbench.jni;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.jni.Callbacks;

/**
 * @author Li Sui
 */
public class CallbacksTest {
	private Callbacks callbacks;

	@Before
	public void setUp() throws Exception {
		callbacks = new Callbacks();
		callbacks.source();
	}

	@After
	public void tearDown() throws Exception {
		callbacks = null;
	}

	/**
	 * Test if Callbacks.source() does call Callbacks.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTarget() {
		Assert.assertTrue(callbacks.TARGET);
	}

	/**
	 * Test if Callbacks.source() does not call Callbacks.target2()
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTarget2() {
		Assert.assertFalse(callbacks.TARGET2);
	}

	/**
	 * Test if Callbacks.source() does not call Callbacks.target(String)
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertFalse(callbacks.TARGET_STRING);
	}
}