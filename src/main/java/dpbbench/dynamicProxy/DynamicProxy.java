package dpbbench.dynamicProxy;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Dynamic proxy. In this scenario, the proxy is supposed to call
 * foo(String), but redirected to target(String) in MyInvocationHandler at runtime.
 *
 * @author Li Sui
 */
public class DynamicProxy {
	public boolean TARGET_STRING = false;
	public boolean TARGET_INTEGER = false;
	public boolean TARGET_NO_PARAM = false;
	public boolean TARGET_FOO = false;

	@Source
	public void source() {
		MyInterface proxy = (MyInterface) Proxy.newProxyInstance(MyInterface.class.getClassLoader(),
				new Class[] { MyInterface.class }, new MyInvocationHandler());
		proxy.foo("hello");
	}

	public interface MyInterface {
		void foo(String s);
	}

	public class MyImpl implements MyInterface {
		@Override
		@Target(expectation = NO)
		public void foo(String s) {
			TARGET_FOO = true;
		}
	}

	@Target(expectation = NO)
	public void target(Integer o) {
		TARGET_INTEGER = true;
	}

	@Target(expectation = NO)
	public void target() {
		TARGET_NO_PARAM = true;
	}

	@Target(expectation = YES)
	public void target(String o) {
		TARGET_STRING = true;
	}

	public class MyInvocationHandler implements InvocationHandler {
		@Override
		public Object invoke(Object obj, Method m, Object[] arg) {
			target((String) arg[0]);
			return null;
		}
	}
}
