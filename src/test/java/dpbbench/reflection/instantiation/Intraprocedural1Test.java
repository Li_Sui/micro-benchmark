package dpbbench.reflection.instantiation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.reflection.instantiation.Intraprocedural1;


/**
 * @author Li Sui
 */

public class Intraprocedural1Test {

	private Intraprocedural1 intraprocedural1;

	@Before
	public void setUp() throws Exception {
		intraprocedural1 = new Intraprocedural1();
		intraprocedural1.source();
	}

	@After
	public void tearDown() throws Exception {
		intraprocedural1 = null;
	}

	/**
	 * Test if Intraprocedural1.source() does access the field
	 * Intraprocedural1$Target
	 */
	@Category(PositiveTests.class)
	@Test
	public void testClassTargetIsInitialised() {
		Assert.assertTrue(intraprocedural1.TARGET);
	}

	/**
	 * Test if Intraprocedural1.source() does not access the field
	 * Intraprocedural1$tegraT
	 */
	@Category(NegativeTests.class)
	@Test
	public void testClasstegraTIsNotInitialised() {
		Assert.assertFalse(intraprocedural1.TARGET2);
	}
}