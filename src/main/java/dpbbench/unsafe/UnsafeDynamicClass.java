package dpbbench.unsafe;

import dpbbench.util.Source;
import dpbbench.util.Utility;

/**
 * Dynamically load class through unsafe. The target class (Target2) is not in the
 * classPath. The class us compiled at runtime 
 * Reference: http://mishadoff.com/blog/java-magic-part-4-sun-dot-misc-dot-unsafe/
 *
 * @author Li Sui
 */
public class UnsafeDynamicClass {

	public static boolean TARGET = false;
	private static UnsafeDynamicClass INSTANCE = null;

	// for each INSTANCE, fields are reset to default.
	public static UnsafeDynamicClass getINSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new UnsafeDynamicClass();
			TARGET = false;
		}
		return INSTANCE;
	}

	@Source
	public void source() throws Exception {
		byte[] content = Utility.compile(this.getClass().getClassLoader(), "unsafe.Target");
		Utility.getUnsafe()
				.defineClass("unsafe.Target", content, 0, content.length, this.getClass().getClassLoader(), null)
				.newInstance();
	}
}
