package dpbbench.reflection.invocation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */
public class MethodOverloadingTest {

	private MethodOverloading methodOverloading;

	@Before
	public void setUp() throws Exception {
		methodOverloading = new MethodOverloading();
		methodOverloading.source();
	}

	@After
	public void tearDown() throws Exception {
		methodOverloading = null;
	}

	/**
	 * Test if MethodOverloading.source() does call
	 * MethodOverloading.target(string)
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertTrue(methodOverloading.TARGET_STRING);
	}

	/**
	 * Test if MethodOverloading.source() does not call
	 * MethodOverloading.target(int)
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamInt() {
		Assert.assertFalse(methodOverloading.TARGET_INT);
	}
}