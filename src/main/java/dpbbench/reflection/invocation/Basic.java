package dpbbench.reflection.invocation;

import dpbbench.util.Expected;
import dpbbench.util.Source;
import dpbbench.util.Target;
import java.lang.reflect.Method;

/**
 * This is a basic usage of reflection invocation
 *
 * @author Li Sui
 */
public class Basic {

	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		Method m = Basic.class.getDeclaredMethod("target", null);
		m.invoke(this, null);
	}

	@Target(expectation = Expected.YES)
	public void target() {
		this.TARGET = true;
	}

	@Target(expectation = Expected.NO)
	public void target2() {
		this.TARGET2 = true;
	}
}
