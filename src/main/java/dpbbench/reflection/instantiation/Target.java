package dpbbench.reflection.instantiation;

import static dpbbench.util.Expected.YES;

public class Target {

    @ dpbbench.util.Target(expectation= YES,belongsTo=dpbbench.reflection.instantiation.Basic2.class)
    public Target(){
        Basic2.TARGET=true;
    }
}
