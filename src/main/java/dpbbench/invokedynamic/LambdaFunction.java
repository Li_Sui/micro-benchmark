package dpbbench.invokedynamic;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Function represents a function that accepts one arguments and produces a
 * result. This scenario is modified to accept no arguments.
 *
 * @require Java 8 and above
 * @author Li Sui
 */
public class LambdaFunction {
	public boolean TARGET_NO_PARAM = false;
	public boolean TARGET_STRING = false;
	public boolean TARGET_INTEGER = false;

	@Source
	public void source() {
		java.util.function.Function<Integer, String> c = (i) -> target();
		c.apply(3);
	}

	@Target(expectation = NO)
	public String target(String input) {
		this.TARGET_STRING = true;
		return null;
	}

	@Target(expectation = NO)
	public String target(Integer input) {
		this.TARGET_INTEGER = true;
		return null;
	}

	@Target(expectation = YES)
	public String target() {
		this.TARGET_NO_PARAM = true;
		return null;
	};
}
