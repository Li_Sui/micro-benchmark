package dpbbench;

import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.runner.RunWith;

/**
 * use ClassPathSuite library (https://github.com/takari/takari-cpsuite) to infer every tests in a Test suite
 */
@RunWith(ClasspathSuite.class)
public class AllTests {
}
