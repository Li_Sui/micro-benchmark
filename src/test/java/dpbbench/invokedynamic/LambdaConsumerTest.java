package dpbbench.invokedynamic;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.invokedynamic.LambdaConsumer;

/**
 * @author Li Sui
 */
public class LambdaConsumerTest {

	private LambdaConsumer lambda;

	@Before
	public void setUp() throws Exception {
		lambda = new LambdaConsumer();
		lambda.source();
	}

	@After
	public void tearDown() throws Exception {
		lambda = null;
	}

	/**
	 * Test if LambdaConsumer.source() does call LambdaConsumer.target();
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetThatHasNoParam() {
		Assert.assertTrue(lambda.TARGET_NO_PARAM);
	}

	/**
	 * Test if LambdaConsumer.source() does not call
	 * LambdaConsumer.target(String);
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertFalse(lambda.TARGET_STRING);
	}
}