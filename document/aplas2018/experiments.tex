\section{Experiments}

\subsection{Methodology}
\label{ssect:methodology}

We conducted an array of experiments with the benchmark. In particular, we were interested to see whether the benchmark examples were suitable to differentiate the capabilities of mainstream static analysis frameworks. We selected three frameworks based on (1) their wide use in the community, evidenced by citation counts of core papers, indicating that the respective frameworks are widely used, and therefore issues in those frameworks will have a wider impact on the research community, (2) the respective frameworks claim to have some support for dynamic language features, in particular  reflection, (3) the respective projects are active, indicating that the features of those frameworks will continue to have an impact. 

Based on those criteria, we evaluated \textit{soot-3.1.0}, \textit{doop}~\footnote{as doop does not release versions, we used a version built from commit 4a94ae3bab4edcdba068b35a6c0b8774192e59eb} and \textit{wala-1.4.3}. For each tool, we considered a basic configuration, and an advanced configuration to switch on support for advanced language features. All three tools have options to switch those features on. This reflects the fact that advanced analysis is not free, but usually comes at the price of precision and scalability. 

Using these analysers, we built call graphs using a mid-precision, context-insensitive variable type analysis. Given the simplicity of our examples, where each method has at most one call site, we did not expect that context sensitivity would have made a difference. To the contrary,  a context-sensitive analysis computes a smaller call graph, and would therefore have reduced the recall of the tool further. On the other hand, a less precise method like CHA could have led to a misleading higher recall caused by the accidental coverage of target methods as FPs.

For \textit{wala}, we used the 0-CFA call graph builder. By default, we set \texttt{com.ibm.\-wa\-la.\-ipa.callgraph.AnalysisOptions.ReflectionOptions} to \texttt{NONE}, in the advanced configuration used, it was set to \texttt{FULL}.

For \textit{soot}, we used spark (\texttt{"cg.spark=enabled,cg.spark=vta"}). For the advanced configuration, we also used the \texttt{"safe-forname"} and the \texttt{"safe-new\-instance"} options. There is another option to support the resolution of reflective call sites, \texttt{types-for-invoke}. Enabling this option leads to an error that was reported, but at the time of writing this issue has not yet been resolved~\footnote{\url{https://groups.google.com/forum/m/#!topic/soot-list/xQwsU7DlmqM}, accessed 5 June 2018}. 

% TODO: enable URL in footnote: https://groups.google.com/forum/m/#!topic/soot-list/xQwsU7DlmqM


For \textit{doop}, we used the following options: \texttt{context-insensitive}, \texttt{ignore-main\-method}, \texttt{only-application-classes-fact-gen}. For the advanced configuration, we also enabled  \texttt{reflection} \texttt{reflection-classic} \texttt{reflection-high-sound\-ness-mode} \texttt{reflection-substring-analysis} \texttt{reflection-invent-un\-known\-obj\-ects} \texttt{reflection-refined-objects} and  \texttt{reflection-speculative-use-based\--ana\-lysis}.

We did not consider any hybrid pre-analysis, such as \textit{tamiflex}~\cite{bodden2011taming}, this was outside the scope of this study. This will be discussed in more detail in section~\ref{ssect:results}.

The experiments were set up as follows: for each benchmark program, we used a lightweight byte code analysis to extract the oracle from the \texttt{@Target} annotations. Then we computed the call graph with the respective static analyser using the method annotated as \texttt{@Source} as entry point, and stored the result in probe format~\cite{lhotak2007comparing}. Finally, using the call graph, we  computed the FPs and FNs of the static call graph with respect to the oracle, using the annotations as the ground truth. For each combination of benchmark program and static analyser, we computed a \textit{result state} depending on the annotations found in the methods reachable from the \texttt{@Source}-annotated method in the computed call graph as defined in Table~\ref{tab:states:det}. For instance, the state ACC (for accurate) means that in the computed call graph, all methods annotated with \texttt{@Target(expectation=YES)} and none of the methods annotated with \texttt{@Target(expectation=NO)} are reachable from the method annotated with \texttt{@Source}. The FP and FN indicate the presence of false positive (imprecision) and false negatives (unsoundness), respectively, the FN+FP state indicates that the results of the static analysis are both unsound and imprecise. Reachable means that there is a path. This is slightly more general than looking for an edge and takes the fact into account that a particular JVM might use intermediate methods to implement a certain dynamic invocation pattern.

\begin{table}[]
	\centering
	\caption{Result state definitions for programs with consistent behaviour across different JVMs}
	\label{tab:states:det}
	\begin{tabular}{lcc}
		\toprule
		result & \multicolumn{2}{c}{methods reachable from source by annotation} \\
		state  & \texttt{@Target(expectation=YES)} & \texttt{@Target(expectation=NO)} \\ \midrule
		ACC   & all                    & none                    \\
		FP     & all                     & some                    \\
		FN     & none                & none                    \\
		FN+FP & none            & some    \\  \bottomrule              
	\end{tabular}
\end{table}

Figure~\ref{fig:first} illustrates this classification. As discussed in Section~\ref{ssect:ambigous}, there are  programs that use the \texttt{@Target(expectation=MAYBE)} annotation, indicating that actual program behaviour is not defined by the specification, and depends on the JVM being used. This is illustrated in Figure~\ref{fig:second}.

For the programs that use the \texttt{@Target(expectation=MAYBE)} annotation, we had to modify this definition according to the semantics of the annotation: during execution, exactly one of these methods will be invoked, but it is up to the particular JVM to decide which one. We define result states as shown in Table~\ref{tab:states:indet}. Note that the \texttt{@Target(expectation=YES)} and the \texttt{@Target(expectation=MAYBE)} annotations are never used for the same program, and there is at most one method annotated with \texttt{@Target(expectation=YES)} in a program.  

This definition is very lenient - we assess the results of a static analyser as sound (ACC or FP) if it does compute a path that links the source with \textit{any} possible target. This means that soundness is defined with respect to the behaviour observed with only \textit{some}, \textit{but not all}, JVMs. 


\begin{figure}%
	\centering
	\subfigure[Consistent behaviour across JVMs]{%
		\label{fig:first}%
		\includegraphics[height=2in]{figure1.pdf}}%
	\qquad
	\subfigure[Inconsistent behaviour across JVMs]{%
		\label{fig:second}%
		\includegraphics[height=2in]{figure2.pdf}}%
	\caption{Observed vs computed call graph}
\end{figure}


\begin{table}[h!]
	\centering
	\caption{Result state definition for programs with behaviour that depends on the JVM}
	\label{tab:states:indet}
	\begin{tabular}{lcc}
		\toprule
		result & \multicolumn{2}{c}{methods reachable from source by annotation} \\
		state & \texttt{@Target(expectation=MAYBE)} & \texttt{@Target(expectation=NO)} \\ \midrule
		ACC   & some                     & none                      \\
		FP    & some                       & some                      \\
		FN    & none                       & none                      \\
		FN+FP & none                    & some                  \\   \bottomrule
	\end{tabular}
\end{table}

\subsection{Reproducing Results}

The benchmark and the scripts used to obtain the results can be found in the following public repository: \url{https://bitbucket.org/Li_Sui/benchmark/}. Further instructions can be found in the repository \texttt{README.md} file.

\subsection{Results and Discussion}
\label{ssect:results}
%Table \ref{tab:experimentResults} shows the results of the benchmark experiment results.
%\todo[inline]{note for the table format: numbers of scenario without Reflecton -numbers of scenario with Reflection}
%\todo[inline]{note for OPAL: Wala fails to run with transformed bytecode. (com.ibm.wala.util.debug.UnimplementedError: com.ibm.wala.shrikeCT.InvalidClassFileException: Class file invalid at 10: bad magic number: 1347093252)
%Soot is able to capture all 3 lambda cases but not dynamo. Doop captures none of them}
%\todo[inline]{note for invokedynamic FN: It is Dynamo}
%\todo[inline]{note for jni ACC: It is Thread}
%\todo[inline]{note for unsafe FN+FP: It is UnsafeTypeConfusion}
%\todo[inline]{note for unsafe FP: it is UnsafeException}
%\todo[inline]{experiment environment:jdk1.8.0\_144, ubuntu 18.04, Intel i7-7500U,2.70GHz x4 , 16G RAM. (Note that doop runs in a virtual vm which has 2G ram, ubuntu 16.04. The problem is logicblox depends on some libraries that are no longer available in ubunut 18.04) Soot:3.1.0-SNAPSHOT wala:1.4.3 doop commit 4a94ae3bab4edcdba068b35a6c0b8774192e59eb probe:1.0 OPALLambdaRectifier: build on 22.12.2017 }

%\todo[inline]{Soot configuration: whole-program, all-reachable, spark enable, vta. \\Soot has a hybrid analysis tool called tamflex to deal with reflection. it is out of the scope. We assume following options "safe-forname" and "safe-newinstance" are reflection analysis. see https://goo.gl/t13VrX  (I think we can remove reflection analysis from soot because we are no longer using Class.forname() to find a class).}
%\todo[inline]{Wala configuration: CHA, ZeroCFA(context-insensitive pointer analysis) \\Reflection analysis:AnalysisOptions.ReflectionOptions.FULL or NONE}
%
%\todo[inline]{Doop configurationn:context-insensitive,ignore-main-method,only-application-classes-fact-gen. \\ Reflection analysis: reflection reflection-classic eflection-high-soundness-mode reflection-substring-analysis reflection-invent-unknown-objects reflection-refined-objects reflection-speculative-use-based-analysis}
%
%\todo[inline]{Note for benchmark: runs under Oracle jdk1.8.0\_144, Openjdk1.8.0\_40 and IBM jdk1.8.0\_171. Scenario dpbbench.reflection.invocation.Interprocedural3, dpbbench.reflection.invocation.ReturnTypeOverloading2 and ...ReturnTypeOverloading6 behave differently with IBM jdk1.8.0\_171}


Results are summarised in Table~\ref{tab:experimentResults}. As expected, none of the static analysers tested handled all features soundly. For \textit{wala} and \textit{doop}, there are significant differences between the plain and the advanced modes. In the advanced mode, both handle simple usage patterns of reflection well, but in some cases have to resort to over-approximation to do so. \textit{Wala} also has support for certain usage patterns of other features: it models \texttt{invokedynamic} instructions generated by the compiler for lambdas correctly, and also models the intermediate native call in \texttt{Thread.start} . This may be a reflection of the maturity and stronger industrial focus of the tool. \textit{Wala} also models the dynamic proxy when in advanced mode. We note however that we did not test \textit{doop} with the new proxy-handling features that were just added very recently~\cite{Fourtounis2018Proxies}. 

While \textit{soot} does not score well, even when using the advanced mode, we note that  \textit{soot} has better integration with \textit{tamiflex} and therefore uses a  fundamentally different approach to soundly model dynamic language features. We did not include this in this study. How well a dynamic (pre-) analysis works depends a lot on the quality (coverage) of the driver, and for the micro-benchmark we have used we can construct a perfect driver. Using soot with \textit{tamiflex}  with such a driver would have yielded excellent results in terms of accuracy, but those results would not have been very meaningful.

None of the frameworks handles any of the \texttt{Unsafe} scenarios well. There is one particular program where all analysers compute the wrong call graph edge: the target method is called on a field that is initialised as \texttt{new Target()}, but between the allocation and the invocation of \texttt{target()} the field value is swapped for an instance of another type using \texttt{Unsafe.putObject}. While this scenario appears far-fetched, we note that \texttt{Unsafe} is widely used in libraries~\cite{mastrangelo2015use}, and has been exploited (see Section~\ref{ssect:cve}).

\begin{table}[h!]
\centering
\caption{Static call graph construction evaluation results, reporting the number of programs with the respective result state, format: (number obtained with basic configuration) / (number obtained with advanced configuration)}
\label{tab:experimentResults}
\begin{tabular}{llcccc}
\toprule
\textbf{Category}                                               & \textbf{Analyser} & \textbf{ACC}  & \textbf{FN}    & \textbf{FP}  & \textbf{FN+FP} \\ \midrule

\multirow{3}{*}{vanilla}                                                        & \textit{soot}     & 1/1  & 0/0   & 0/0 & 0/0   \\  
                                                                                & \textit{wala}     & 1/1  & 0/0   & 0/0 & 0/0   \\ 
                                                                                & \textit{doop}     & 1/1  & 0/0   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{reflection}                                                     & \textit{soot}     & 0/1  & 12/11 & 0/0 & 0/0   \\ 
                                                                                & \textit{wala}     & 0/4  & 12/3  & 0/5 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 12/8  & 0/4 & 0/0   \\ \midrule
\multirow{3}{*}{\begin{tabular}[c]{@{}l@{}}dynamic\\ class loading\end{tabular}} & \textit{soot}    & 0/0  & 1/1   & 0/0 & 0/0   \\ 
                                                                                & \textit{wala}     & 0/0  & 1/1   & 0/0 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 1/1   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{dynamic proxy}                                                   & \textit{soot}    & 0/0  & 1/1   & 0/0 & 0/0   \\ 
                                                                                & \textit{wala}     & 0/1  & 1/0   & 0/0 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 1/1   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{invokedynamic}                                                  & \textit{soot}     & 0/0  & 4/4   & 0/0 & 0/0   \\  
                                                                                & \textit{wala}     & 3/3  & 1/1   & 0/0 & 0/0   \\ 
                                                                                & \textit{doop}     & 0/0  & 4/4   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{JNI}                                                            & \textit{soot}     & 1/1  & 1/1   & 0/0 & 0/0   \\  
                                                                                & \textit{wala}     & 1/1  & 1/1   & 0/0 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 2/2   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{serialisation}                                                  & \textit{soot}     & 1/1  & 0/0   & 0/0 & 0/0   \\  
                                                                                & \textit{wala}     & 1/1  & 0/0   & 0/0 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 1/1   & 0/0 & 0/0   \\ \midrule
\multirow{3}{*}{Unsafe}                                                         & \textit{soot}     & 0/0  & 2/2   & 1/1 & 1/1   \\  
                                                                                & \textit{wala}     & 0/0  & 2/2   & 1/1 & 1/1   \\  
                                                                                & \textit{doop}     & 0/0  & 2/2   & 1/1 & 1/1   \\ \midrule                                                                            \multirow{3}{*}{reflection-ambiguous}                                           & \textit{soot}     & 0/0  & 2/2   & 0/0 & 0/0   \\ 
                                                                                & \textit{wala}     & 0/0  & 2/0   & 0/2 & 0/0   \\  
                                                                                & \textit{doop}     & 0/0  & 2/1   & 0/1 & 0/0   \\ \bottomrule
\end{tabular}

\end{table}


% \begin{table}[h]
% \centering
% \begin{tabular}{l|l|c|c|c|c}
% \toprule
% \textbf{Category}                                                                        & \textbf{Analyser} & \textbf{ACC}  & \textbf{FN}    & \textbf{FP}  & \textbf{FN+FP} \\ \midrule

% \multirow{3}{*}{Vanilla}                                                        & Soot     & 1-1  & 0-0   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 1-1  & 0-0   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 1-1  & 0-0   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{Reflection}                                                     & Soot     & 0-0  & 18-18 & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 0-10 & 18-4  & 0-4 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-6  & 18-9  & 0-3 & 0-0   \\ \hline
% \multirow{3}{*}{\begin{tabular}[c]{@{}l@{}}Dynamic\\ Class Loading\end{tabular}} & Soot     & 0-0  & 1-1   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 0-0  & 1-1   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 1-1   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{Dynamic Proxy}                                                   & Soot     & 0-0  & 1-1   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 0-1  & 1-0   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 1-1   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{invokedynamic}                                                  & Soot     & 0-0  & 4-4   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 3-3  & 1-1   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 4-4   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{JNI}                                                            & Soot     & 0-0  & 2-2   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 1-1  & 1-1   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 2-2   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{Serialisation}                                                  & Soot     & 1--1 & 0-0   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Wala     & 1-1  & 0-0   & 0-0 & 0-0   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 1-1   & 0-0 & 0-0   \\ \hline
% \multirow{3}{*}{Unsafe}                                                         & Soot     & 0-0  & 2-2   & 1-1 & 1-1   \\ \cline{2-6} 
%                                                                                 & Wala     & 0-0  & 2-2   & 1-1 & 1-1   \\ \cline{2-6} 
%                                                                                 & Doop     & 0-0  & 2-2   & 1-1 & 1-1   \\ \bottomrule
% \end{tabular}
% \caption{Benchmark Experiment (numbers indicates number of scenarios without and with reflection enabled)}
% \label{tab:experimentResults}
% \end{table}

