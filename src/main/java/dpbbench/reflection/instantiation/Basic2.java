package dpbbench.reflection.instantiation;

import dpbbench.util.Source;

/**
 * Soot has option "safe-forname":assume any static initializer could be executed
 * and "safe-fornewinstance" : assume that any constructor could be executed when the program calls Class.newInstance()
 * Compare with Basic1, this scenario instantiates Target class that is not an INNER class
 *
 * @author Li Sui
 */
public class Basic2 {
    public static boolean TARGET=false;
    private static Basic2 INSTANCE = null;

    // for each INSTANCE, fields are reset to default.
    public static Basic2 getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new Basic2();
            TARGET = false;
        }
        return INSTANCE;
    }

    @Source
    public void source() throws Exception{
        Class claz =Target.class;
        claz.newInstance();
    }
}
