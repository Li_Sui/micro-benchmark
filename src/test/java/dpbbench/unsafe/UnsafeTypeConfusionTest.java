package dpbbench.unsafe;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.unsafe.UnsafeTypeConfusion;

/**
 * @author Li Sui
 */

public class UnsafeTypeConfusionTest {
	private UnsafeTypeConfusion unsafeTypeConfusion;

	@Before
	public void setUp() throws Exception {
		unsafeTypeConfusion = new UnsafeTypeConfusion();
		unsafeTypeConfusion.source();
	}

	@After
	public void tearDown() throws Exception {
		unsafeTypeConfusion = null;
	}

	/**
	 * Test if UnsafeTypeConfusion.source() does call
	 * UnsafeTypeConfusion$Target2.target();
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetInClassTarget2() {
		Assert.assertTrue(unsafeTypeConfusion.TARGET2);
	}

	/**
	 * Test if UnsafeTypeConfusion.source() does not call
	 * UnsafeTypeConfusion$Target.target();
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetInClassTarget() {
		Assert.assertFalse(unsafeTypeConfusion.TARGET);
	}
}