package dpbbench.vanilla;

import dpbbench.NegativeTests;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.PositiveTests;
import dpbbench.vanilla.Vanilla;

/**
 * @author Li Sui
 */
public class VanillaTest {
	private Vanilla vanilla;

	@Before
	public void setUp() throws Exception {
		vanilla = new Vanilla();
		vanilla.source();
	}

	@After
	public void tearDown() throws Exception {
		vanilla = null;
	}

	/**
	 * Test if vanilla.source() does call vanilla.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testTargetMethodBeenCalled() {
		Assert.assertTrue(vanilla.TARGET);
	}

	/**
	 * Test if vanilla.source() does call vanilla.target(int o)
	 */
	@Category(NegativeTests.class)
	@Test
	public void testTarget2MethodHasNotBeenCalled() {
		Assert.assertFalse(vanilla.TARGET2);
	}


}