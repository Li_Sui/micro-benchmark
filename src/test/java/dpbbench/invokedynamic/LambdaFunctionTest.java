package dpbbench.invokedynamic;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.invokedynamic.LambdaFunction;

/**
 * @author Li Sui
 */
public class LambdaFunctionTest {

	private LambdaFunction lambda;

	@Before
	public void setUp() throws Exception {
		lambda = new LambdaFunction();
		lambda.source();
	}

	@After
	public void tearDown() throws Exception {
		lambda = null;
	}

	/**
	 * Test if LambdaFunction.source() does call LambdaFunction.target();
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetThatHasNoParam() {
		Assert.assertTrue(lambda.TARGET_NO_PARAM);
	}

	/**
	 * Test if LambdaFunction.source() does not call
	 * LambdaFunction.target(String);
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertFalse(lambda.TARGET_STRING);
	}

	/**
	 * Test if LambdaFunction.source() does not call
	 * LambdaFunction.target(Integer);
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamInteger() {
		Assert.assertFalse(lambda.TARGET_INTEGER);
	}

}