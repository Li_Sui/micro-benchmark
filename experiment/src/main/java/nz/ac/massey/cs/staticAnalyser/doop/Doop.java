package nz.ac.massey.cs.staticAnalyser.doop;

import probe.CallEdge;
import probe.ObjectManager;
import probe.ProbeMethod;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;

/**
 * Doop call graph construction
 * @author li sui
 */
public class Doop {

    private probe.CallGraph probeCG;

    /**
     *
     * @param doopResultFilePath  use pa-datalog to query result and then produces a txt file.
     */
    public void run(String doopResultFilePath) throws Exception{
        build(doopResultFilePath);
    }

    private void build(String doopResultFilePath) throws Exception{
        probeCG=new probe.CallGraph();
        BufferedReader br =  new BufferedReader(new FileReader(doopResultFilePath));
        String line = "";
        String splitBy = ",\\s";
        while ((line = br.readLine()) != null) {
            ProbeMethod source =parseMethod(line.split(splitBy)[0]);
            ProbeMethod target=parseMethod(line.split(splitBy)[1]);
            probe.CallEdge probeEdge = new CallEdge(source,target) ;
            probeCG.entryPoints().add(source);
            ((Set<CallEdge>)probeCG.edges()).add(probeEdge);
        }
        br.close();
    }

    private ProbeMethod parseMethod(String method){
        method=method.trim();
        String firstPart=method.split("\\s")[0];

        String secondPart=method.split("\\s")[1];
        String thirdPart=method.split("\\s")[2];
        String declaredClass=firstPart.substring(1,firstPart.length()-1);

        String signature=secondPart+" "+thirdPart.substring(0,thirdPart.length()-1);
        String name=thirdPart.substring(0,thirdPart.indexOf("("));

        return ObjectManager.v().getMethod(ObjectManager.v()
                .getClass(declaredClass),name,signature);
    }

    public probe.CallGraph getProbeCG(){
        return this.probeCG;
    }
}
