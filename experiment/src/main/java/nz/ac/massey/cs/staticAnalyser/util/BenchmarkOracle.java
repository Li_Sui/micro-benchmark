package nz.ac.massey.cs.staticAnalyser.util;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;


import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import dpbbench.util.Expected;
import dpbbench.util.Source;
import dpbbench.util.Target;
import probe.CallEdge;
import probe.ObjectManager;
import probe.ProbeMethod;

import java.lang.reflect.Method;
import java.util.*;

public class BenchmarkOracle {
   private String packageName;

   public BenchmarkOracle(String packageName){
       this.packageName=packageName;
   }

   public probe.CallGraph getExpectedCG() throws Exception{
       Multimap<String, BenchmarkMethodAdaptor> methodMap= build();
       probe.CallGraph cg =new probe.CallGraph();
       for (String benchmark : methodMap.keySet()) {

           ProbeMethod source=null;
           ProbeMethod target=null;
           for (BenchmarkMethodAdaptor method : methodMap.get(benchmark)) {
                if(method.isSource){
                    source=method.getMethod();
                }else {
                    if (method.getExpected().equals(Expected.YES)) {
                        target = method.getMethod();
                    }
                }
           }

           if(source!=null && target!=null){
               cg.entryPoints().add(source);
               ((Set<CallEdge>)cg.edges()).add(new probe.CallEdge(source,target));
           }
       }

       addAdditionEdges(cg);
       //For now, we consider all maybe targets are expected edges
       cg.edges().addAll(getMaybeCG().edges());
       return cg;
   }

    //Get weakly accurate call graph (Expected.MAYBE)
    public probe.CallGraph getMaybeCG() throws Exception{
        Multimap<String, BenchmarkMethodAdaptor> methodMap= build();
        probe.CallGraph cg =new probe.CallGraph();
        for (String benchmark : methodMap.keySet()) {
            ProbeMethod source=null;
            ProbeMethod maybeTarget=null;
            for (BenchmarkMethodAdaptor method : methodMap.get(benchmark)) {
                if(method.isSource){
                    source=method.getMethod();
                    cg.entryPoints().add(source);
                    break;
                }
            }

            for (BenchmarkMethodAdaptor method : methodMap.get(benchmark)) {
                if(!method.isSource) {
                    if (method.getExpected().equals(Expected.MAYBE)) {
                        maybeTarget = method.getMethod();
                        ((Set<CallEdge>) cg.edges()).add(new probe.CallEdge(source, maybeTarget));
                    }
                }
            }
        }
        return cg;
    }


   public probe.CallGraph getUnexpectedCG() throws Exception{
       Multimap<String, BenchmarkMethodAdaptor> methodMap= build();
       probe.CallGraph cg =new probe.CallGraph();
       for (String benchmark : methodMap.keySet()) {
           ProbeMethod source=null;
           ProbeMethod unexpectedTarget=null;
           for (BenchmarkMethodAdaptor method : methodMap.get(benchmark)) {
               if(method.isSource){
                   source=method.getMethod();
                   cg.entryPoints().add(source);
                   break;
               }
           }

           for (BenchmarkMethodAdaptor method : methodMap.get(benchmark)) {
               if(!method.isSource) {
                   if (method.getExpected().equals(Expected.NO)) {
                       unexpectedTarget = method.getMethod();
                       ((Set<CallEdge>) cg.edges()).add(new probe.CallEdge(source, unexpectedTarget));
                   }
               }
           }
       }
       return cg;
   }



    /**
     * build all methods(source, expected target and unexpected target methods) in benchmark
     * Note that CustomClassLoader and UnsafeDynamicClass's target constructor is not in the class path, manually added to this collector
     *
     * @return
     * @throws Exception
     */
    public Multimap<String, BenchmarkMethodAdaptor> build() throws Exception {
        // get all classes from class path
        List<String> classes =new FastClasspathScanner(packageName).strictWhitelist().scan()
                .getNamesOfAllStandardClasses();

        Multimap<String, BenchmarkMethodAdaptor> methodMap = ArrayListMultimap.create();
        for (String c : classes) {
            Class<?> claz =Class.forName(c);
            //for each annotated method
            for (java.lang.reflect.Method method : claz.getDeclaredMethods()) {
                addAnnotatedMethodToMap(method,methodMap);
            }

            //for each annotated constructor
            for(java.lang.reflect.Constructor constructor: claz.getConstructors()){
                addAnnotatedConstructorToMap(constructor,methodMap);
            }
        }

        return methodMap;
    }

    private void addAnnotatedConstructorToMap(java.lang.reflect.Constructor constructor,Multimap<String, BenchmarkMethodAdaptor> map){
        String declareClass = constructor.getDeclaringClass().getName();
        String paramType =constructParamType(constructor.getGenericParameterTypes());
        String constructorName="<init>";
        String signature= "void "+constructorName+"("+paramType+")";

        if (constructor.isAnnotationPresent(Target.class)) {
            BenchmarkMethodAdaptor target =new BenchmarkMethodAdaptor( ObjectManager.v().getMethod(ObjectManager.v()
                    .getClass(declareClass),constructorName,signature));
            target.setExpected(constructor.getDeclaredAnnotation(Target.class).expectation());
            //if the target class is an inner class, map to its outer class
            if(constructor.getDeclaredAnnotation(Target.class).belongsTo()==Object.class) {
                if (declareClass.contains("$")) {
                    map.put(declareClass.split("\\$")[0], target);
                } else {
                    map.put(declareClass, target);
                }
            }else{
                map.put(constructor.getDeclaredAnnotation(Target.class).belongsTo().getName(),target);
            }
        }
    }


    private void addAnnotatedMethodToMap(Method method,Multimap<String, BenchmarkMethodAdaptor> map){
        String declareClass = method.getDeclaringClass().getName();
        String returnType = method.getGenericReturnType().toString().replace("interface ","").replace("class ","");
        String paramType=constructParamType(method.getGenericParameterTypes());
        String methodName = method.getName();
        String signature= returnType+" "+methodName+"("+paramType+")";

        if (method.isAnnotationPresent(Source.class)) {
            BenchmarkMethodAdaptor source =new BenchmarkMethodAdaptor( ObjectManager.v().getMethod(ObjectManager.v()
                    .getClass(declareClass),methodName,signature));
            source.setSource(true);
            map.put(declareClass,source);
        }

        if (method.isAnnotationPresent(Target.class)) {
            BenchmarkMethodAdaptor target = new BenchmarkMethodAdaptor(ObjectManager.v().getMethod(ObjectManager.v()
                    .getClass(declareClass), methodName, signature));
            target.setExpected(method.getDeclaredAnnotation(Target.class).expectation());
            //if the target method is not specified which benchmark it belongs to, map the value defined in belongsTo
            if(method.getDeclaredAnnotation(Target.class).belongsTo()==Object.class) {
                //if the target class is an inner class, map to its outer class
                if (declareClass.contains("$")) {
                    map.put(declareClass.split("\\$")[0], target);
                } else {
                    map.put(declareClass, target);
                }
            }else{
                map.put(method.getDeclaredAnnotation(Target.class).belongsTo().getName(),target);
            }
        }
    }

    private String constructParamType(Object[] types){
        StringBuilder paramType = new StringBuilder();
        for (int i = 0; i < types.length; i++) {
            paramType.append(types[i].toString().replace("interface ","").replace("class ",""));
            if(i<types.length-1){
                paramType.append(",");
            }
        }
        return paramType.toString();
    }

    //CustomClassLoader and UnsafeDynamicClass's target constructor is not in the class path, manually added to this collector
    //TODO: make it configurable
    private void addAdditionEdges(probe.CallGraph cg){

        probe.CallEdge edge1=new probe.CallEdge(
                ObjectManager.v().getMethod(ObjectManager.v().getClass("dpbbench.dynamicClassLoading.CustomClassLoader"),"source","void source()"),
                ObjectManager.v().getMethod(ObjectManager.v().getClass("dynamicClassLoading.Target"),"<init>","void <init>()")
        );


        probe.CallEdge edge2=new probe.CallEdge(
                ObjectManager.v().getMethod(ObjectManager.v().getClass("dpbbench.unsafe.UnsafeDynamicClass"),"source","void source()"),
                ObjectManager.v().getMethod(ObjectManager.v().getClass("unsafe.Target"),"<init>","void <init>()")
        );

        ((Set<CallEdge>)cg.edges()).add(edge1);
        ((Set<CallEdge>)cg.edges()).add(edge2);
    }



    //used for adapting a method
    private class BenchmarkMethodAdaptor{
        ProbeMethod method;
        Expected expected;
        boolean isSource;

        BenchmarkMethodAdaptor(ProbeMethod method){
            this.method=method;
        }

        public Expected getExpected() {
            return expected;
        }

        public void setExpected(Expected expected) {
            this.expected = expected;
        }

        public boolean isSource() {
            return isSource;
        }

        public void setSource(boolean source) {
            isSource = source;
        }

        public ProbeMethod getMethod() {
            return method;
        }
    }
}
