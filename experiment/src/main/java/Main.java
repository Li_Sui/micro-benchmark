import nz.ac.massey.cs.staticAnalyser.ExperimentHarness;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * @author li sui
 */
public class Main {
    final static String  applicationName = "benchmark experiment";
    public static void main(String[] args){
        if (args.length < 1) {
            printUsage(applicationName , constructOptions(), System.out);
        }
        //test
        parseArgs(args);
    }

    public static void parseArgs(final String[] commandLineArguments){
        final CommandLineParser cmdLineParser = new DefaultParser();
        final Options options = constructOptions();
        CommandLine commandLine;

        try{
            commandLine = cmdLineParser.parse(options, commandLineArguments);
            String path="benchmark.jar";
            String analyser="all";
            Boolean reflection=false;
            boolean opal=false;

            if ( commandLine.hasOption("help") ) {
                printUsage(applicationName, constructOptions(), System.out);
            }
            if(commandLine.hasOption("path")){
                path=commandLine.getOptionValue("path");
                if(!new File(path).exists()){
                    System.err.println("file not exist");
                    return;
                }
            }
            if(commandLine.hasOption("analyser")){
                analyser=commandLine.getOptionValue("analyser");
            }
            if(commandLine.hasOption("reflection")){
                reflection=(commandLine.getOptionValue("reflection").equals("on"))? true: false;
            }
            if(commandLine.hasOption("opal")){
                opal=(commandLine.getOptionValue("opal").equals("on"))? true: false;
            }

            ExperimentHarness.runExperiment(analyser, path, reflection, opal);

        }catch (Exception parseException){
            System.err.println("Encountered exception while parsing using Parser:\n"
                                + parseException.getMessage() );
        }
    }

    public static void printUsage(final String applicationName,final Options options,final OutputStream out) {
        final PrintWriter writer = new PrintWriter(out);
        final HelpFormatter usageFormatter = new HelpFormatter();
        usageFormatter.printUsage(writer, 80, applicationName, options);
        writer.flush();
    }

    public static Options constructOptions(){
        final Options options = new Options();
        options.addOption("help",false,"list of available options");
        options.addOption("reflection", true,"toggle reflection option for each analyser, default:off");
        options.addOption("path", true,"path to target jar");
        options.addOption("analyser" ,true,"apply a specific analyser. available options are doop,soot,wala and all. default:all");
        options.addOption("opal", true,"enable opal to transform invokedynamic. default:off");
        return options;
    }
}
