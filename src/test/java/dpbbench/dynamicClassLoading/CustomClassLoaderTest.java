package dpbbench.dynamicClassLoading;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.PositiveTests;
import dpbbench.dynamicClassLoading.CustomClassLoader;

/**
 * @author li Sui
 */
public class CustomClassLoaderTest {

	private CustomClassLoader customClassLoader;

	@Before
	public void setUp() throws Exception {
		customClassLoader = CustomClassLoader.getINSTANCE();
		customClassLoader.source();
	}

	@After
	public void tearDown() throws Exception {
		customClassLoader = null;
	}

	/**
	 * test if CustomClassLoader.source() loads Target which is not in the
	 * classPath
	 */
	@Category(PositiveTests.class)
	@Test
	public void testLoadingClassTarget() {
		Assert.assertTrue(customClassLoader.TARGET);
	}
}