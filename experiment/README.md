# benchmark-experiment


[raw data access](https://drive.google.com/drive/folders/1pgGHyfOIWTrtRgOFNUfPOs-tKj-e9WDp?usp=sharing)


run Main in any IDEs by providing following parameters: 

-path MY.JAR (default: benchmark.jar in root dir)

-analyser soot|wala|doop|all (default:all )

-reflection on|off (default:off)

-opal on|off (default:off)  note that opal does not work for wala!

A summary table will be printed at the end and raw data saved under result/

###### Note 

To run on the command line, benchmark.jar must be in the class path.

We included Doop produced callgraph in the repository, named "resultReflection.txt" and "resultWithoutReflection.txt".
To reproduce them. 

* You need to checkout [Doop repo](https://bitbucket.org/yanniss/doop/). As doop does not release versions, we used a version built from commit 
4a94ae3bab4edcdba068b35a6c0b8774192e59eb
* Download [pa-datalog(logicbox)](http://snf-705535.vm.okeanos.grnet.gr/agreement.html)
* Follow instruction to install pa-datalog.
* Configure environment
```
export JAVA_HOME=...path to your JDK
export LOGICBLOX_HOME=/opt/lb/pa-datalog/logicblox
export DOOP_HOME=...path to doop home
export DOOP_PLATFORMS_LIB=$DOOP_HOME
export PATH=$DOOP_HOME/bin:$LOGICBLOX_HOME/bin:$JAVA_HOME/bin:$PATH
```
* Run 
```
source /opt/lb/pa-datalog/lb-env-bin.sh
```
* Place jce.jar, jsse.jar,rt.jar,tools.jar in  $DOOP_HOME/JREs/jre1.7/lib
* To generate CG without reflection option enabled. CD to $DOOP_HOME
```
./doop --platform java_7 -a context-insensitive --lb 
--ignore-main-method -only-application-classes-fact-gen
-i benchmark.jar
```
Use following query to get the call graph
```
_path(?fromMethod, ?toMethod) <-
     CallGraphEdge(_,?invocation, _,?toMethod),
     Instruction:Method[?invocation] = ?fromMethod.
```
* To generate CG with reflection option enabled.CD to $DOOP_HOME
```
./doop --platform java_7 -a context-insensitive --lb --ignore-main-method 
-only-application-classes-fact-gen --reflection --reflection-classic 
--reflection-high-soundness-mode --reflection-substring-analysis 
--reflection-invent-unknown-objects --reflection-refined-objects 
--reflection-speculative-use-based-analysis -i benchmark.jar 
```
Use following query to get the call graph
```
_path(?fromMethod, ?toMethod) <-
  CallGraphEdge(_,?invocation, _,?toMethod),
  Instruction:Method[?invocation] = ?fromMethod.
  
_path(?fromMethod, ?toMethod) <-
  ReflectiveMethodInvocation(_,?invocation,?toMethod),
  Instruction:Method[?invocation] = ?fromMethod.
```
To run the query in logicblox, save above query in a file named callgrph.logic, and run
```
bloxbatch -db last-analysis -query -file callgraph.logic  > result.txt
```
The call graph is saved in a file named result.txt
