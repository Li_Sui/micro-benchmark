package dpbbench.jni;

import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Basic Thread usage. Thread.start()->Thread.start0() which is a native method
 * interface
 *
 * @author Li Sui
 */
public class Thread implements Runnable {

	public boolean TARGET = false;

	@Override
	public void run() {
		target();
	}

	@Target(expectation = YES)
	public void target() {
		TARGET = true;
	}

	@Source
	public void source() {
		java.lang.Thread t = new java.lang.Thread(this);
		t.start();
	}
}
