package dpbbench;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Run tests when irrelevant targets are not called
 * 
 * @author Li Sui
 */
@RunWith(Categories.class)
@Categories.IncludeCategory(NegativeTests.class)
@Suite.SuiteClasses({ AllTests.class })
public class NegativeTestSuite {
}
