package dpbbench.reflection.instantiation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.reflection.instantiation.Interprocedural2;


/**
 * @author Li Sui
 */

public class Interprocedural2Test {

	private Interprocedural2 interprocedural2;

	@Before
	public void setUp() throws Exception {
		interprocedural2 = new Interprocedural2();
		interprocedural2.source();
	}

	@After
	public void tearDown() throws Exception {
		interprocedural2 = null;
	}

	/**
	 * Test if Interprocedural2.source() does call the constructor in
	 * Interprocedural2$Target
	 */
	@Category(PositiveTests.class)
	@Test
	public void testClassTargetIsInitialised() {
		Assert.assertTrue(interprocedural2.TARGET);
	}

	/**
	 * Test if Interprocedural2.source() does not call the constructor in
	 * Interprocedural2$Target2
	 */
	@Category(NegativeTests.class)
	@Test
	public void testClassTarget2IsNotInitialised() {
		Assert.assertFalse(interprocedural2.TARGET2);
	}
}