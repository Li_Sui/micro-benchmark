package dpbbench.reflection.invocation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;

import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Interprocedural usage: method name is supplied externally.
 *
 * @author Li Sui
 */
public class Interprocedural1 {

	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		BufferedReader br = new BufferedReader(
				new FileReader(this.getClass().getClassLoader().getResource("method.txt").getFile()));
		String methodName = br.readLine();
		Method m = Interprocedural1.class.getDeclaredMethod(methodName, null);
		m.invoke(this, null);
	}

	@Target(expectation = NO)
	public void target2() {
		this.TARGET2 = true;
	}

	@Target(expectation = YES)
	public void target() {
		this.TARGET = true;
	}

}
