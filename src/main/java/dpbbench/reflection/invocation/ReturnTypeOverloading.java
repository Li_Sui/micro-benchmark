package dpbbench.reflection.invocation;

import java.lang.reflect.Method;
import dpbbench.util.Source;

/**
 * According to the Oracle Java API spec: "If more than one method with the same
 * parameter types is declared in a class, and one of these methods has a return
 * type that is more specific than any of the others, that method is returned;
 * otherwise one of the methods is chosen arbitrarily." 
 * Source: https://goo.gl/TRLcFg
 * 
 * But IBM JRE violates this, and returns the first method that encounters
 * (Collection target()) .
 *
 * The byte code is engineered through ASM framework (http://asm.ow2.io/).
 * source code is shown below
 *
 * @Target(expectation=Expected.NO)
 * public Collection target(){
 *   this.TARGET_COLLECTION =true;
 *   return null; }
 * 
 * @Target(expectation=Expected.YES)
 * public List target() {
 * 	 this.TARGET_LIST =true;
 * 	 return null; }
 *
 * @author Li Sui
 */
public class ReturnTypeOverloading {
	public ReturnTypeOverloadingHelper helper = new ReturnTypeOverloadingHelper();

	@Source
	public void source() throws Exception {
		Method m = ReturnTypeOverloadingHelper.class.getDeclaredMethod("target");
		m.invoke(helper, null);
	}
}
