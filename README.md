# A Micro-Benchmark for Dynamic Program Behaviour in Java

This micro-benchmark consists of small Java programs that use certain dynamic language features that are difficult to model by 
static analysis tools. The structure of the respective programs is highly canonical, using [convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration). 

One program in the benchmark is `Vanilla` -- this actually does not contain any dynamic language feature but serves as baseline. 
It is also useful to explain the structure of each program, and the conventions being used. 

```java
public class Vanilla {
	public boolean TARGET=false;
		@Source
		public void source(){
			target();
		}
		@Target(expectation=YES)
		public void target(){
			this.TARGET =true;
		}
        @Target(expectation = NO)
        public void target(int o) {
            this.TARGET2 = true;
        }
	}
}
```

The idea is to use those programs to assess how static analysis tools model dynamic features, in particular, what the statically extracted callgraph looks like.
The code is simple, and also encodes (by means of annotations that can be easily processed) an oracle manually extracted from the observed program behaviour. 
Due to the simplicity of this particular program, the expected
behaviour is obvious: the invocation of `source()` should trigger the invocation of `target()`. 

But there are also matching test cases to actually observe program behaviour when the program is exercises (via junit), like this one:

```java
    public class VanillaTest {
        private Vanilla vanilla;
    
        @Before
        public void setUp() throws Exception {
            vanilla=new Vanilla();
            vanilla.source();
        }
        @Category(PositiveTests.class)
        @Test
        public void testTargetMethodBeenCalled() {
            Assert.assertTrue(vanilla.TARGET);
        }	
        @Category(NegativeTests.class)
        @Test
        public void testTarget2MethodHasNotBeenCalled() {
            Assert.assertFalse(vanilla.TARGET2); 
        }
    }
```

While this is trivial, there are multiple language features in Java that result in invocation chains that are not as easy to model, 
the obvious one being (various flavours of) *reflection*, but also including *dynamic proxies*, *dynamic class loading*, *invokedynamic*, *serialization*, features in 
*sun.misc.Unsafe* and *native code (JNI)*. 

While many real-world static analyses tackle this on a framework level (e.g. model the spring framework), the purpose of this benchmark is to facilitate experiments on how a particular static callgraph construction handles the actual Java language / platform features. The benchmark is designed to faciliate experiments 
for both false negatives and false positives (e.g. whether an analyser achieves soundness by over-approximating the callgraph).

## More Info

A presentation we gave at the [BenchWork18 workshop](https://conf.researchr.org/track/issta-2018/benchwork-2018-talks): [https://goo.gl/EV7N1K](https://goo.gl/EV7N1K), describing the benchmark and summarising the findings of a study comparing the doop, soot and wala using the benchmark.

## Notes 

1. for executing environment: `dpbbench.jni.CallBacksTest` may fail if you run it on Mac OS or Windows. The native code used in this example is currently only compiled for Linux distributions.
2. when executing the benchmark using the J9 JVM we experienced some unexpected behaviour due to a bug. This was [reported](https://github.com/eclipse/openj9/issues/2162) and has now been [fixed](https://github.com/eclipse/openj9/pull/2240).  But note that some scenarios behave differently on different JVMs. Please check the [BenchWork18 talk](https://goo.gl/EV7N1K) for a discussion.
3. You can find an experiment conduct on this benchmark under experiment/ 

## Building and Versioning

The project can be built with Maven. 

We may update the benchmark occasionally. We will use tags to create new versions, please checkout the respective tag. 



