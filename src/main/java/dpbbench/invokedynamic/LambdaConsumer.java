package dpbbench.invokedynamic;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Consumer interface represents an operation that accepts a single input
 * argument and returns no result. This scenario is modified to accept no
 * arguments.
 * 
 * @require Java 8 and above
 * @author Li Sui
 */
public class LambdaConsumer {

	public boolean TARGET_NO_PARAM = false;
	public boolean TARGET_STRING = false;

	@Source
	public void source() {
		java.util.function.Consumer<String> c = (input) -> target();
		c.accept("input");
	}

	@Target(expectation = NO)
	public void target(String input) {
		this.TARGET_STRING = true;
	}

	@Target(expectation = YES)
	public void target() {
		this.TARGET_NO_PARAM = true;
	}

}
