package dpbbench.reflection.invocation;

import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;


/**
 * @author Li Sui
 */
public class BasicTest {

    private Basic basic;

    @Before
    public void setUp() throws Exception {
        basic=new Basic();
        basic.source();
    }

    @After
    public void tearDown() throws Exception {
        basic=null;
    }

    /**
     * Test Basic.source() does call Basic.target()
     */
    @Category(PositiveTests.class)
    @Test
    public void testMethodTarget() {
        Assert.assertTrue(basic.TARGET);
    }

    /**
     * Test Basic.source() does not call Basic.target2()
     */
    @Category(NegativeTests.class)
    @Test
    public void testMethodTarget2() { Assert.assertFalse(basic.TARGET2);}
}