package dpbbench.reflection.instantiation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */

public class ConstructorOverloadingTest {
	private ConstructorOverloading constructorOverloading;

	@Before
	public void setUp() throws Exception {
		constructorOverloading = new ConstructorOverloading();
		constructorOverloading.source();
	}

	@After
	public void tearDown() throws Exception {
		constructorOverloading = null;
	}

	/**
	 * Test if ConstructorOverloading.source() does call the constructor in
	 * ConstructorOverloading$Target.<init>(String)
	 */
	@Category(PositiveTests.class)
	@Test
	public void testClassTargetIsInitialised() {
		Assert.assertTrue(constructorOverloading.TARGET);
	}

	/**
	 * Test if ConstructorOverloading.source() does not call the constructor in
	 * ConstructorOverloading$Target2.<init>(Integer)
	 */
	@Category(NegativeTests.class)
	@Test
	public void testClassTarget2IsNotInitialised() {
		Assert.assertFalse(constructorOverloading.TARGET2);
	}
}