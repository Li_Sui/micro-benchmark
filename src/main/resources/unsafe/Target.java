package unsafe;

import dpbbench.util.Expected;

/**
 * used for dpbbench.unsafe.UnsafeDynamicClass
 *
 */
public class Target{
    @dpbbench.util.Target(belongsTo =dpbbench.unsafe.UnsafeDynamicClass.class, expectation= Expected.YES)
    public Target(){
        dpbbench.unsafe.UnsafeDynamicClass.TARGET =true;
    }
}