package dpbbench.reflection.instantiation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;

/**
 * Interprocedural usage: class name is supplied via a different procedural.
 *
 * @author Li Sui
 */
public class Interprocedural2 {
	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		Class claz = Class.forName(new ClassNameProvider().getClassName());
		claz.getConstructor(Interprocedural2.class).newInstance(this);
	}

	public class ClassNameProvider {
		public String getClassName() {
			return "dpbbench.reflection.instantiation.Interprocedural2$Target";
		}
	}

	public class Target {
		@dpbbench.util.Target(expectation = YES)
		public Target() {
			TARGET = true;
		}
	}

	public class Target2 {
		@dpbbench.util.Target(expectation = NO)
		public Target2() {
			TARGET2 = true;
		}
	}
}
