#include <stdio.h>
#include <jni.h>
#include "dpbbench_jni_Callbacks.h"

JNIEXPORT void JNICALL
Java_dpbbench_jni_Callbacks_source(JNIEnv *env, jobject obj)
{
  jclass cls = (*env)->GetObjectClass(env, obj);
  jmethodID mid = (*env)->GetMethodID(env, cls, "target", "()V");
  if (mid == 0) {
    return;
  }
  (*env)->CallVoidMethod(env, obj, mid);
}
