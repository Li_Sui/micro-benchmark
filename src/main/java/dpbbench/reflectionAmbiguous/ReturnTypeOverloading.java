package dpbbench.reflectionAmbiguous;

import java.lang.reflect.Method;

import dpbbench.util.Source;

/**
 * Return type overloading, The order of method matters
 * the byte code is engineered through ASM framework (http://asm.ow2.io/). source code is shown below
 *
	 @Target(expectation=Expected.NO)
	 public Set target(){
		 this.TARGET_SET=true;
		 return null;
	 }
	 @Target(expectation=Expected.MAYBE)
	 public List target() {
		 this.TARGET_LIST = true;
		 return null;
	 }
 * The analyser reports accurate results w.r.t. the observable edges for some,
 * but not all, tested JVMs. They are Oracle JRE 1.8.0 144, Openjdk JRE 1.8.0 40 and IBM JRE 1.8.0 171.
 * @author li sui
 */
public class ReturnTypeOverloading {
    public ReturnTypeOverloadingHelper helper =new ReturnTypeOverloadingHelper();

    @Source
    public void source() throws Exception{
        Method m = ReturnTypeOverloadingHelper.class.getDeclaredMethod("target");
        m.invoke(helper, null);
    }
}
