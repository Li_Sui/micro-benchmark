package dpbbench.reflection.invocation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;
import java.lang.reflect.Method;

/**
 * Method overloading.
 * @author Li Sui
 */

public class MethodOverloading {

    public boolean TARGET_INT=false;
    public boolean TARGET_STRING=false;

    @Source
    public void source() throws Exception{
        Method m = MethodOverloading.class.getDeclaredMethod("target", new Class[]{String.class});
        m.invoke(this, "a");
    }

    @Target(expectation=NO)
    public void target(int a){
        this.TARGET_INT =true;
    }

    @Target(expectation=YES)
    public void target(String a){
        this.TARGET_STRING =true;
    }
}