package dpbbench.unsafe;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;
import dpbbench.util.Utility;

/**
 * using unsafe to call target method without object initialisation
 * 
 * @author Li Sui
 */
public class UnsafeInitialization {

	public static boolean TARGET = false;
	public static boolean INNER_INIT = false;
	public boolean TARGET2 = false;

	private static UnsafeInitialization instance = null;

	// for each instance, fields are reset to default.
	public static UnsafeInitialization getInstance() {
		if (instance == null) {
			instance = new UnsafeInitialization();
			TARGET = false;
			INNER_INIT = false;
		}
		return instance;
	}

	@Source
	public void source() throws Exception {
		UnsafeInitialization.Inner inner = (Inner) Utility.getUnsafe().allocateInstance(Inner.class);
		inner.target();
	}

	@Target(expectation = NO)
	public void target() {
		this.TARGET2 = true;
	}

	public class Inner {
		@Target(expectation = NO)
		public Inner() {
			INNER_INIT = true;
		}

		@Target(expectation = YES)
		public void target() {
			TARGET = true;
		}
	}
}
