package dpbbench.reflection.instantiation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import java.io.BufferedReader;
import java.io.FileReader;
import dpbbench.util.Source;

/**
 * Interprocedural usage: class name is supplied externally.
 *
 * @author Li Sui
 */
public class Interprocedural1 {
	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		BufferedReader br = new BufferedReader(
				new FileReader(this.getClass().getClassLoader().getResource("class.txt").getFile()));
		Class claz = Class.forName(br.readLine());
		claz.getConstructor(Interprocedural1.class).newInstance(this);
	}

	public class Target {
		@dpbbench.util.Target(expectation = YES)
		public Target() {
			TARGET = true;
		}
	}

	public class Target2 {
		@dpbbench.util.Target(expectation = NO)
		public Target2() {
			TARGET2 = true;
		}
	}
}
