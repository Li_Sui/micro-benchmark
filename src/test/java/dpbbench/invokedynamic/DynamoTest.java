package dpbbench.invokedynamic;

import dpbbench.invokedynamic.client.DynamoClient;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * Binary code is provided in src/main/resources/dynamo.jar.
 * ref:https://goo.gl/wr7BVj In this scenario, the target method is invoked via
 * custom invokedynamic instruction
 *
 * DynamoClient{ void source(){ new DynamoTarget().target("hello"); } }
 *
 * DynamoTarget{ void target(String o){}; void target(){}; }
 *
 * @require Java 8 and above
 * @author Li Sui
 */
public class DynamoTest {
	private DynamoClient client;

	@Before
	public void setUp() throws Exception {
		client = new DynamoClient();
		client.source();
	}

	@After
	public void tearDown() throws Exception {
		client = null;
	}

	/**
	 * Test if DynamoClient.source() does call DynamoTarget.target(String);
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertTrue(client.DYNAMO.TARGET);
	}

	/**
	 * Test if DynamoClient.source() does not call DynamoTarget.target();
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetThatHasNoParam() {
		Assert.assertFalse(client.DYNAMO.TARGET2);
	}
}
