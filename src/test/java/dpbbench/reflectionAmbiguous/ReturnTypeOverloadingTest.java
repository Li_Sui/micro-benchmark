package dpbbench.reflectionAmbiguous;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */
public class ReturnTypeOverloadingTest {
	private ReturnTypeOverloading returnTypeOverloading;

	@Before
	public void setUp() throws Exception {
		returnTypeOverloading = new ReturnTypeOverloading();
		returnTypeOverloading.source();
	}

	@After
	public void tearDown() throws Exception {
		returnTypeOverloading = null;
	}

	/**
	 * Test if ReturnTypeOverloading.source() does call
	 * <Set>ReturnTypeOverloadingHelper.target() which comes before
	 * <List>ReturnTypeOverloadingHelper.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetReturnSet() {
		Assert.assertTrue(returnTypeOverloading.helper.TARGET_SET);
	}

	/**
	 * Test if ReturnTypeOverloading.source() does not call
	 * <List>ReturnTypeOverloadingHelper.target()
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetReturnList() {
		Assert.assertFalse(returnTypeOverloading.helper.TARGET_LIST);
	}
}