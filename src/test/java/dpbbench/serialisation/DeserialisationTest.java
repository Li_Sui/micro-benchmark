package dpbbench.serialisation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.serialisation.Deserialisation;

/**
 * @author Li Sui
 */
public class DeserialisationTest {

	private Deserialisation der;

	@Before
	public void setUp() throws Exception {
		der = Deserialisation.getINSTANCE();
		der.source();
	}

	@After
	public void tearDown() throws Exception {
		der = null;
	}

	/**
	 * Test if Deserialisation.source() does call Deserialisation$Target.target();
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetInClassTarget() {
		Assert.assertTrue(der.TARGET);
	}

	/**
	 * Test if Deserialisation.source() does not call
	 * Deserialisation$Target2.target();
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetInClassTarget2() {
		Assert.assertFalse(der.TARGET2);
	}

}