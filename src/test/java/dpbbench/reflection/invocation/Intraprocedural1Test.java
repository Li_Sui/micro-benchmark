package dpbbench.reflection.invocation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */
public class Intraprocedural1Test {

	private Intraprocedural1 inter;

	@Before
	public void setUp() throws Exception {
		inter = new Intraprocedural1();
		inter.source();
	}

	@After
	public void tearDown() throws Exception {
		inter = null;
	}

	/**
	 * Test if Intraprocedural1.source() does call Intraprocedural1.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTarget() {
		Assert.assertTrue(inter.TARGET);
	}

	/**
	 * Test if Intraprocedural1.source() does not call Intraprocedural1.TEGRAT()
	 * where the method name is reserved.
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTEGRAT() {
		Assert.assertFalse(inter.TARGET2);
	}
}