package dpbbench.reflection.instantiation;

import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * @author Li Sui
 */
public class Basic1Test {
	private Basic1 basic1;

	@Before
	public void setUp() throws Exception {
		basic1 = new Basic1();
		basic1.source();
	}

	@After
	public void tearDown() throws Exception {
		basic1 = null;
	}

	/**
	 * Test if Basic1.source() does call the constructor in Basic$Target
	 */
	@Category(PositiveTests.class)
	@Test
	public void testClassTargetIsInitialised() {
		Assert.assertTrue(basic1.TARGET);
	}

	/**
	 * Test if Basic1.source() does not call the constructor in Basic$Target2
	 */
	@Category(NegativeTests.class)
	@Test
	public void testClassTarget2IsNotInitialised() {
		Assert.assertFalse(basic1.TARGET2);
	}
}