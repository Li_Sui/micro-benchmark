package dpbbench.reflection.instantiation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;

/**
 * Constructor overloading
 *
 * @author li sui
 */
public class ConstructorOverloading {
	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		Class claz = Target.class;
		claz.getConstructor(new Class[] { ConstructorOverloading.class, String.class }).newInstance(this, "hello");
	}

	public class Target2 {
		@dpbbench.util.Target(expectation = NO)
		public Target2(Integer c) {
			TARGET2 = true;
		}
	}

	public class Target {
		@dpbbench.util.Target(expectation = YES)
		public Target(String c) {
			TARGET = true;
		}
	}
}
