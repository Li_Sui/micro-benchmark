package dpbbench.serialisation;

import java.io.*;
import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Utility;

/**
 * Deserialisation scenario.
 *
 * @author Li Sui
 */
public class Deserialisation implements Serializable {

	public static boolean TARGET = false;
	public static boolean TARGET2 = false;
	private static Deserialisation INSTANCE = null;

	// for each INSTANCE, fields are reset to default.
	public static Deserialisation getINSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new Deserialisation();
			TARGET = false;
			TARGET2 = false;
		}
		return INSTANCE;
	}

	@Source
	public void source() throws Exception {
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(Utility.serialise(new Target())));
		TargetInterface foo = (TargetInterface) ois.readObject();
		foo.target();
		ois.close();
	}

	public interface TargetInterface {
		void target();
	}

	public class Target implements TargetInterface, Serializable {
		@Override
		@dpbbench.util.Target(expectation = YES)
		public void target() {
			TARGET = true;
		}
	}

	public class Target2 implements TargetInterface, Serializable {
		@Override
		@dpbbench.util.Target(expectation = NO)
		public void target() {
			TARGET2 = true;
		}
	}
}
