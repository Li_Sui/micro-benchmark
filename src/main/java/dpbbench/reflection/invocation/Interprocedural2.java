package dpbbench.reflection.invocation;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import java.lang.reflect.Method;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Interprocedural usage: method name is supplied via a different procedural
 *
 * @author Li Sui
 */
public class Interprocedural2 {

	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		String methodName = new MethodNameProvider().getMethodName();
		Method m = Interprocedural2.class.getDeclaredMethod(methodName, null);
		m.invoke(this, null);
	}

	@Target(expectation = NO)
	public void target2() {
		this.TARGET2 = true;
	}

	@Target(expectation = YES)
	public void target() {
		this.TARGET = true;
	}

	public class MethodNameProvider {
		public String getMethodName() {
			return "target";
		}
	}
}
