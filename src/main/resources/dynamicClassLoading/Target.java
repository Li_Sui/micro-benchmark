package dynamicClassLoading;

import dpbbench.util.Expected;

/**
 * and dpbbench.dynamicClassLoading.CustomClassLoader
 */
public class Target{
    @dpbbench.util.Target(belongsTo=dpbbench.dynamicClassLoading.CustomClassLoader.class,expectation= Expected.YES)
    public Target(){
        dpbbench.dynamicClassLoading.CustomClassLoader.TARGET=true;
    }
}