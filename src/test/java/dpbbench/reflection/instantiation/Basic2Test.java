package dpbbench.reflection.instantiation;


import dpbbench.PositiveTests;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * @author Li Sui
 */
public class Basic2Test {
    private Basic2 basic2=null;

    @Before
    public void setUp() throws Exception {
        basic2=Basic2.getINSTANCE();
        basic2.source();
    }

    @After
    public void tearDown() throws Exception {
        basic2=null;
    }

    /**
     * Test if Basic2.source() does call the constructor in dpbbenchmark.reflection.instantiation.Target
     */
    @Category(PositiveTests.class)
    @Test
    public void testClassTargetIsInitialised() {
        Assert.assertTrue(basic2.TARGET);
    }
}