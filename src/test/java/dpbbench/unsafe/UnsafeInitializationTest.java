package dpbbench.unsafe;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.unsafe.UnsafeInitialization;

/**
 * @author Li Sui
 */
public class UnsafeInitializationTest {

	private UnsafeInitialization unsafe;

	@Before
	public void setUp() throws Exception {
		unsafe = UnsafeInitialization.getInstance();
		unsafe.source();
	}

	@After
	public void tearDown() throws Exception {
		unsafe = null;
	}

	/**
	 * Test if UnsafeInitialization.source() does call
	 * UnsafeInitialization.Inner.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testInnerMethodTarget() {
		Assert.assertTrue(unsafe.TARGET);
	}

	/**
	 * Test if UnsafeInitialization.source() does not call the constructor of
	 * UnsafeInitialization.Inner
	 */
	@Category(NegativeTests.class)
	@Test
	public void testInnerClassInitialization() {
		Assert.assertFalse(unsafe.INNER_INIT);
	}

	/**
	 * Test if UnsafeInitialization.source() does not call
	 * UnsafeInitialization.target()
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTarget() {
		Assert.assertFalse(unsafe.TARGET2);
	}

}