package dpbbench.unsafe;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;
import dpbbench.util.Utility;

/**
 * unsafe can throw a checked Exception without declaring it
 * 
 * @author Li Sui
 */
public class UnsafeException {
	public boolean EXCEPTION = false;
	public boolean TARGET = false;

	@Source
	public void source() {
		Utility.getUnsafe().throwException(new CustomException());
		target();
	}

	@Target(expectation = NO)
	public void target() {
		TARGET = true;
	}

	public class CustomException extends Exception {
		@Target(expectation = YES)
		public CustomException() {
			super();
			EXCEPTION = true;
		}
	}
}
