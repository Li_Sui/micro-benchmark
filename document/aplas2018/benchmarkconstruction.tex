\section{The Benchmark}

\subsection{Benchmark Structure}

The benchmark is organised as a Maven~\footnote{\url{https://maven.apache.org/}, accessed 30 August 2018} project using the standard project layout. The actual programs are organised in name spaces (packages) reflecting their category. Programs are minimalistic, and their behaviour is in most cases easy to understand for an experienced programmer by ``just looking at the program''. All programs have a \texttt{source()} method  and one or more other methods, usually named \texttt{target(..)}. 

Each program has an integrated oracle of expected program behaviour, encoded using standard Java annotations. Methods annotated with \texttt{@Source} are call graph sources: we consider the program behaviour triggered by the execution of those methods from an outside client. Methods annotated  with \texttt{@Target} are methods that may or may not be invoked directly or indirectly from a call site in the method annotated with \texttt{@Source}.  The expectation whether a target method is to be invoked or not is encoded in the \texttt{@Target} annotation's \texttt{expectation} attribute that can be one of three values: \texttt{Expected.YES} -- the method is expected to be invoked , \texttt{Expected.NO} -- the method is not expected to be invoked, or \texttt{Expected.MAYBE} -- exactly one of the methods with this annotation is expected to be invoked, but which one may depend on the JVM to be used. For each program, \textit{either} exactly one method is annotated with \texttt{@Target(expectation=Expected.YES)}, \textit{or} some methods are annotated with \texttt{@Target(expectation=Expected.MAYBE}. 

The benchmark contains a \texttt{Vanilla} program that defines the base case: a single source method that has a call site where  the target method is invoked using a plain \texttt{invokevirtual} instruction. The annotated example is shown in Listing~\ref{code:vanilla}, this also illustrates the use of the oracle annotations.

\begin{lstlisting}[language=Java,label={code:vanilla},caption=Vanilla program source code (simplified)]
public class Vanilla {
	public boolean TARGET = false;
	public boolean TARGET2 = false;
	@Source public void source(){
			target();
	}
	@Target(expectation = YES) public void target(){
			this.TARGET = true;
	}
	@Target(expectation = NO) public void target(int o) {
			this.TARGET2 = true;
	}	
}
\end{lstlisting}

The main purpose of the annotations is to facilitate the set up of experiments with static analysers. Since the annotations have a retention policy that makes them visible at runtime, the oracle to test static analysers can be easily inferred from the benchmark program. In particular, the annotations can be used to test for both FNs (soundness issues) and FPs (precision issues).

In Listing~\ref{code:vanilla}, the target method changes the state of the object by setting the \texttt{TARGET} flag. The purpose of this feature is to make invocations easily observable, and to confirm actual program behaviour by means of executing the respective programs by running a simple client implemented as a junit test. Listing \ref{code:vanilla-test} shows the respective test for \texttt{Vanilla} -- we expect that after an invocation of \texttt{source()} by the test driver, \texttt{target()} will have been called after \texttt{source()} has returned , and we check this with an assertion check on the \texttt{TARGET} field. We also tests for methods that should not be called, by checking that the value of the respective field remains \texttt{false}. 

\begin{lstlisting}[language=Java,label={code:vanilla-test},caption=Vanilla test case (simplified)]
public class VanillaTest {
	private Vanilla vanilla;
	@Before public void setUp() throws Exception {
		vanilla = new Vanilla();
		vanilla.source();
	}
	@Test public void testTargetMethodBeenCalled() {
		Assert.assertTrue(vanilla.TARGET);
	}
	@Test public void testTarget2MethodHasNotBeenCalled() {
		Assert.assertFalse(vanilla.TARGET2);
	}
}

\end{lstlisting}


\subsection{Dynamic Language Features and Vulnerabilities}
\label{ssect:cve}

One objective for benchmark construction was to select features that are of interest to static program analysis, as there are known vulnerabilities that exploit those features. Since the discussed features allow bypassing Java's security model, which relies on information-hiding, memory and type safety, Java security vulnerabilities involving their use have been reported that have implications ranging from attacks on confidentiality, integrity and the availability of applications. 
Categorised under the Common Weakness Enumeration (CWE) classification, untrusted deserialisation, unsafe reflection, type confusion, untrusted pointer dereferences and buffer overflow vulnerabilities are the most notable.  

CVE-2015-7450 is a well-known serialisation vulnerability in the Apache Commons Collections library. It lets an attacker execute arbitrary commands on a system that uses unsafe Java deserialisation. Use of reflection is common in vulnerabilities as discussed by Holzinger et al~\cite{holzinger2016depth} where the authors discover that 28  out of 87 exploits studied utilised reflection vulnerabilities. An example is CVE-2013-0431, affecting the Java JMX API, which allows loading of arbitrary classes and invoking their methods. 
%CVE-2012-5076 is another vulnerability involving reflection where a malicious app can use a \texttt{getMethod} object in a vulnerable class to indirectly access the reflection API. 
CVE-2009-3869, CVE-2010-3552, CVE-2013-08091 are buffer overflow vulnerabilities involving the use of native methods. As for vulnerabilities that use the \texttt{Unsafe} API, CVE-2012-0507 is a vulnerability in \texttt{AtomicReferenceArray} which uses \texttt{Unsafe} to store a reference in an array directly that can violate type safety and permit escaping the sandbox. CVE-2016-4000 and CVE-2015-3253 reported for Jython and Groovy are due to serialisable invocation handlers for proxy instances. While we are not aware of vulnerabilities that exploit invokedynamic directly, there are several CVEs that exploit the method handle API used in the invokedynamic bootstrapping process, including CVE-2012-5088, CVE-2013-2436 and CVE-2013-0422.

The following subsections contain a high-level discussion of the various categories of programs in the benchmark. A detailed discussion of each program is not possible within the page limit, the reader is referred to the benchmark repository for more details.

\subsection{Reflection}
\label{ssect:reflection}

Java's reflection protocol is widely used and it is the foundation for many frameworks. With reflection, classes can be dynamically instantiated, fields can be accessed and manipulated, and methods can be invoked.  How easily reflection can be modelled by a static analysis highly depends on the \textit{usage context}. In particular, a reflective call site for \textit{Method.invoke} can be easily handled if the parameter at the method access site (i.e., the call site of \texttt{Class.getMethod} or related methods) are known, for instance, if method name and parameter types can be inferred. Existing static analysis support is based on this wider idea. 

However, this is not always possible. The data needed to accurately identify an invoked method might be supplied by other methods (therefore, the static analysis must be inter-procedural to capture this), only partially available (e.g., if only the method name can safely be inferred, a static analysis may decide to over-approximate the call graph and create edges for all possible methods with this name), provided through external resources (a popular pattern in enterprise frameworks like spring, service loaders, or JEE web applications), or some custom procedural code.  All of those usage patterns do occur in practice~\cite{li2014self,landman2017challenges}, and while  exotic uses of reflection might be rare, they are also the most interesting ones as they might be used in the kind of vulnerabilities static analysis is interested to find.

The benchmark examples reflect this range of usage patterns from trivial to sophisticated. Many programs overload the target method, this is used to test whether a static analysis tool achieves sound reflection handling at the price of precision. 

\subsection{Reflection with Ambiguous Resolution}
\label{ssect:ambigous}

As discussed in Section~\ref{sec:background}, we also consider scenarios where a program is (at least partially) not generated by  \texttt{javac}. Since at byte code level methods are identified by a combination of name and descriptor, the JVM supports return type overloading, and the compiler uses this, for instance, in order to support co-variant return types~\cite[sect. 8.4.5]{gosling2014java} by generating bridge methods. This raises the question how the methods in \texttt{java.lang.Class} used to locate methods resolve ambiguity as they use only name and parameter types, but not the return type, as parameters.  According to the respective class documentation, ``If more than one method with the same parameter types is declared in a class, and one of these methods has a return type that is more specific than any of the others, that method is returned; otherwise one of the methods is chosen arbitrarily''~\footnote{\url{https://goo.gl/JG9qD2}, accessed 24 May 2018}. In case of return type overloading used in bridge methods, this rule still yields an unambiguous result, but one can easily engineer byte code where the arbitrary choice clause applies. The benchmark contains a respective example, \texttt{dpbbench.ambiguous.ReturnTypeOverloading}. There are two target methods, one returning \texttt{java.util.Set} and one returning \texttt{java.util.List}. Since neither return type is a subtype of the other type, the JVM is free to choose either.  In this case we use the \texttt{@Target (expectation=MAYBE)} annotation to define the oracle. We acknowledge that the practical relevance of this might be low at the moment, but we included this scenario as it highlights that the concept of possible program behaviour used as ground truth to assess the soundness of static analysis is not as clear as it is widely believed. Here, possible program executions can be defined either with respect to all or some JVMs.  

It turns out that Oracle JRE 1.8.0\_144 / OpenJDK JRE 1.8.0\_40 on the one hand and IBM JRE 1.8.0\_171 on the other hand actually do select different methods here. We have also observed that IBM JRE 1.8.0\_171 chooses the incorrect method in the related \texttt{dpbbench.\-reflection.invocation.\-Return\-Type\-Overloading} scenario (note the different package name). In this scenario, the overloaded target methods return \texttt{java.\-util.\-Collection} and \texttt{java.util.List}, respectively, and the IBM JVM dispatches to the method returning \texttt{java.util.\-Collection} in violation of the rule stipulated in the API specification. We reported this as a bug, and it was accepted and fixed report~\footnote{\url{https://github.com/eclipse/openj9/pull/2240}, accessed 16 August 2018}.


A similar situation occurs when the selection of the target method depends on the order of annotations returned via the reflective API. This scenario does occur in practice, for instance, the use of this pattern in the popular \textit{log4j} library is discussed in \cite{sui2017use}. The reflection API does not  impose constraints on the order of annotations returned by \texttt{java.lang.reflect.Method.getDeclaredAnnotations()}, therefore, programs have different possible executions for different JVMs. 

% Jens: I removed TARGET3 , target3 as this is not relevant for the point beingmade here, note the simplified in the caption

\begin{lstlisting}[language=Java,label={code:anno},caption=Example where the selection of the target method depends on the JVM being used (simplified)]
public class Invocation {
	public boolean TARGET = false;
	public boolean TARGET2 = false;
	@Retention(RUNTIME) @Target(METHOD) @interface Method{}
	@Source public void source() throws Exception {
		for (Method method:Invocation.class.getDeclaredMethods()){
				if (method.isAnnotationPresent(Method.class)){
					method.invoke(this, null);
					return;
	}	}	} }
	@Method @Target (expectation=MAYBE) public void target(){
		this.TARGET =true;
	}
	@Method	@Target (expectation=MAYBE) public void target2(){
		this.TARGET2 =true;
	}
}
\end{lstlisting}
	
When executing those two examples and recording the actual call graphs, we observe that the call graphs differ depending on the JVM being used. For instance, in the program in Listing~\ref{code:anno}, the target method selected at the call site in \texttt{source()} is \texttt{target()} for both Oracle JRE 1.8.0\_144 and OpenJDK JRE 1.8.0\_40 , and \texttt{target2()} for IBM JRE 1.8.0\_171. 

\subsection{Dynamic Classloading}

Java distinguishes between classes and class loaders. This can be used to dynamically load, or even generate classes at runtime. This is widely used in practice, in particular for frameworks that compile embedded scripting or domain-specific languages ``on the fly'', such as Xalan~\footnote{\url{https://xalan.apache.org}, accessed 4 June 2018}. 

There is a single example in the benchmark that uses a custom classloader to load and instantiate a class. The constructors of the respective class are the expected target methods.

\subsection{Dynamic Proxies}

Dynamic proxies were introduced in Java 1.3, they are similar to protocols like Smalltalk's \texttt{doesNotUnderstand}, they capture calls to unimplemented methods via an invocation handler.  A major application is to facilitate distributed object frameworks like CORBA and RMI, but dynamic proxies are also used in mock testing frameworks. For example, in the \textit{XCorpus} dataset of 75 real-world programs, 13 use dynamic proxies~\cite{dietrich2017xcorpus} (implement \texttt{InvocationHandler} and have call sites for \texttt{Proxy.newProxyInstance}).  Landman et all observed that ``all [state-of-the-art static analysis] tools assume .. absence of Proxy classes''~\cite{landman2017challenges}. 
%Support for proxies has been investigated for \textit{doop} only very recently~\cite{Fourtounis2018Proxies}.

The benchmark contains a single program in the \texttt{dynamicProxy} category. In this program, the source method invokes an interface method \texttt{foo()} through an invocation handler. In the invocation handler, \texttt{target(String)} is invoked. The \texttt{target} method is overloaded in order to test the precision of the analysis.

\subsection{Invokedynamic}

The \texttt{invokedynamic} instruction was introduced in Java 7. It gives the user more control over the method dispatch process by using a user-defined bootstrap method that computes the call target. While the original motivation behind  \texttt{invokedynamic} was to provide support for dynamic languages like Ruby, its main (and in the OpenJDK 8, only) application is to provide support for lambdas. In OpenJDK 9, \texttt{invokedynamic} is also used for string concatenation~\cite{JEP280}.

For known usage contexts, support for \texttt{invokedynamic} is possible. If \texttt{invoke\-dynamic} is used with the \texttt{LambdaMetafactory}, then a tool can rewrite this byte code, for instance, by using an alternative byte code sequence that compiles lambdas using anonymous inner classes. The Opal byte code rectifier~\cite{OPAL} is based on this wider idea, and can be used as a standalone pre-processor for static analysis. The rewritten byte code can then be analysed ``as usual''.

The benchmark contains three examples defined by Java sources with different uses of lambdas. The fourth examples is engineered from byte code and is an adapted version of the dynamo compiler example from~\cite{jezek2016magic}. Here, \texttt{invokedynamic} is used for a special compilation of component boundary methods in order to improve binary compatibility. The intention of including this example is to distinguish between \texttt{invokedynamic} for particular usage patterns, and general support for \texttt{invokedynamic}. 

\subsection{Serialisation}

Java serialisation is a feature that is used in order to export object graphs to streams, and vice versa. This is a highly controversial feature, in particular after a large number of serialisation-related vulnerabilities were reported in recent years~\cite{holzinger2016depth,dietrich2017evil}.  

The benchmark contains a single program in this category that relates to the fact that (de-)serialisation offers an extra-linguistic mechanism to construct objects, avoiding constructors. The scenario constructs an object from a stream, and then invokes a method on this object. The client class is not aware of the actual type of the receiver object, as the code contains no allocation site.


\subsection{JNI}

The Java Native Interface (JNI) is a framework that enables Java to call and be called by native applications. There are two programs using JNI in the benchmark. The first scenario uses a custom \texttt{Runnable} to be started by \texttt{Thread.start}. In the Java 8 (OpenJDK 8), \texttt{Runnable.run} is invoked by \texttt{Thread.start} via an intermediate native method \texttt{Thread.start0()}.  This is another scenario that can be handled by static analysis tools that can deal with common usage patterns, rather than with the general feature. The second program is a custom example that uses a grafted method implemented in C. 

\subsection{sun.misc.Unsafe}

The class \texttt{sun.misc.Unsafe} (unsafe for short) offers several low level APIs that can bypass constraints built into standard APIs.  Originally intended to facilitate the implementation of platform APIs, and to provide an alternative for JNI, this  feature is now widely used outside the Java platform libraries~\cite{mastrangelo2015use}.  The benchmark contains four programs in this category,  (1) using unsafe to load a class (\texttt{defineClass}), (2) to throw an exception (\texttt{throwException}), (3) to allocate an instance (\texttt{allocateInstance}) and (4) to swap references (\texttt{putObject}, \texttt{object\-Field\-Off\-set}). 

