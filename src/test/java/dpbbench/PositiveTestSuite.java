package dpbbench;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Run tests when the desire targets are called
 * 
 * @author Li Sui
 */
@RunWith(Categories.class)
@Categories.IncludeCategory(PositiveTests.class)
@Suite.SuiteClasses({ AllTests.class })
public class PositiveTestSuite {
}
