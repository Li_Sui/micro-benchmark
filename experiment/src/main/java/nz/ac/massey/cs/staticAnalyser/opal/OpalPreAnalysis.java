package nz.ac.massey.cs.staticAnalyser.opal;

import org.opalj.br.analyses.Project;
import org.opalj.support.tools.ProjectSerializer;

import java.io.File;

public class OpalPreAnalysis {
    public static void transform(String input, File outputDir) {
        Project<java.net.URL> project = Project.apply(new File(input));
        ProjectSerializer.serialize(project,outputDir);
    }
}
