package dpbbench.reflection.invocation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */
public class Interprocedural2Test {
	private Interprocedural2 inter;

	@Before
	public void setUp() throws Exception {
		inter = new Interprocedural2();
		inter.source();
	}

	@After
	public void tearDown() throws Exception {
		inter = null;
	}

	/**
	 * Test if Interprocedural2.source() does call Interprocedural2.target()
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTarget() {
		Assert.assertTrue(inter.TARGET);
	}

	/**
	 * Test if Interprocedural2.source() does not call Interprocedural2.target2()
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTarget2() {
		Assert.assertFalse(inter.TARGET2);
	}
}