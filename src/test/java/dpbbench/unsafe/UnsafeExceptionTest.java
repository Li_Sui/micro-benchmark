package dpbbench.unsafe;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.unsafe.UnsafeException;

/**
 * @author Li Sui
 */
public class UnsafeExceptionTest {
	private UnsafeException unsafeException;

	@Before
	public void setUp() {
		unsafeException = new UnsafeException();
		try {
			unsafeException.source();
		} catch (Exception e) {
		}
	}

	@After
	public void tearDown() throws Exception {
		unsafeException = null;
	}

	/**
	 * Test uncheck Exception
	 */
	@Category(PositiveTests.class)
	@Test
	public void testUncheckedException() {
		Assert.assertTrue(unsafeException.EXCEPTION);
	}

	/**
	 * Test UnsafeException.source() does not call UnsafeException.target()
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTarget() {
		Assert.assertFalse(unsafeException.TARGET);
	}
}