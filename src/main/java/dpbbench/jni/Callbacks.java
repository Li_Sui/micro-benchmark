package dpbbench.jni;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import java.io.File;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Java method callback through JNI API How to build: Linux: javac -h .
 * dpbbench/jni/Callbacks.java -> generate header file gcc -fPIC
 * -I"$JAVA_HOME/include" -I"$JAVA_HOME/include/linux" -shared -o callbackJNI.so
 * Callbacks.c -> generate lib Windows: TODO MacOS: TODO
 * 
 * @author Li Sui
 */
public class Callbacks {

	/**
	 * FIXME: test breaks after refactoring. needs to change the package name
	 * (import nz.ac.massey.cs.benchmark.util.Expecte => dpbbench.util.Expecte)
	 * in callbackJNI.so
	 */
	/**
	 * FIXME: needs also to check all package qualifiers in the other resource
	 * files
	 */

	static {
		String os =System.getProperty("os.name").toLowerCase();
		if(os.contains("win")){
			System.err.println("we do not support windows");

		}else if(os.contains("osx")){
			System.err.println("we do not support mac os");

		}else if(os.contains("nix") || os.contains("nux")){
			System.load(new File("src/main/resources/callbackJNI.so").getAbsolutePath());
		}
	}

	public boolean TARGET = false;
	public boolean TARGET2 = false;
	public boolean TARGET_STRING = false;

	@Source
	public native void source();

	@Target(expectation = YES)
	public void target() {
		this.TARGET = true;
	}

	@Target(expectation = NO)
	public void target(String a) {
		this.TARGET_STRING = true;
	}

	@Target(expectation = NO)
	public void target2() {
		this.TARGET2 = true;
	}
}
