package dpbbench.reflectionAmbiguous;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;

/**
 * @author Li Sui
 */
public class InvocationTest {
    private Invocation inter;
    @Before
    public void setUp() throws Exception {
        inter= new Invocation();
        inter.source();
    }

    @After
    public void tearDown() throws Exception {
        inter=null;
    }

    /**
     * Test if Invocation.source() does call Invocation.target2()
     */
    @Category(PositiveTests.class)
    @Test
    public void testMethodTarget2() {
        Assert.assertTrue(inter.TARGET2);
    }

    /**
     * Test if Invocation.source() does not call Invocation.target()
     */
    @Category(NegativeTests.class)
    @Test
    public void testMethodTarget() {
        Assert.assertFalse(inter.TARGET);
    }

    /**
     * Test if Invocation.source() does not call Invocation.target3()
     */
    @Category(NegativeTests.class)
    @Test
    public void testMethodTarget3() {
        Assert.assertFalse(inter.TARGET3);
    }
}