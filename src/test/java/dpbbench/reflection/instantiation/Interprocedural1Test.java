package dpbbench.reflection.instantiation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.reflection.instantiation.Interprocedural1;

/**
 * @author Li Sui
 */
public class Interprocedural1Test {
	private Interprocedural1 interprocedural1;

	@Before
	public void setUp() throws Exception {
		interprocedural1 = new Interprocedural1();
		interprocedural1.source();
	}

	@After
	public void tearDown() throws Exception {
		interprocedural1 = null;
	}

	/**
	 * Test if Interprocedural1.source() does call the constructor in
	 * Interprocedural1$Target
	 */
	@Category(PositiveTests.class)
	@Test
	public void testClassTargetIsInitialised() {
		Assert.assertTrue(interprocedural1.TARGET);
	}

	/**
	 * Test if Interprocedural1.source() does not call the constructor in
	 * Interprocedural1$Target2
	 */
	@Category(NegativeTests.class)
	@Test
	public void testClassTarget2IsNotInitialised() {
		Assert.assertFalse(interprocedural1.TARGET2);
	}
}