package dpbbench.dynamicProxy;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.dynamicProxy.DynamicProxy;

/**
 * @author Li Sui
 */
public class DynamicProxyTest {
	private DynamicProxy dynamicProxy;

	@Before
	public void setUp() throws Exception {
		dynamicProxy = new DynamicProxy();
		dynamicProxy.source();
	}

	@After
	public void tearDown() throws Exception {
		dynamicProxy = null;
	}

	/**
	 * Test if DynamicProxy.source() does call DynamicProxy.target(String);
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertTrue(dynamicProxy.TARGET_STRING);
	}

	/**
	 * Test if DynamicProxy.source() does not call DynamicProxy.target(Integer);
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetWithParamInteger() {
		Assert.assertFalse(dynamicProxy.TARGET_INTEGER);
	}

	/**
	 * Test if DynamicProxy.source() does not call DynamicProxy.target();
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetThatHasNoParam() {
		Assert.assertFalse(dynamicProxy.TARGET_NO_PARAM);
	}

	/**
	 * Test if DynamicProxy.source() does not call
	 * DynamicProxy$MyImpl.foo(String)
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodFoo() {
		Assert.assertFalse(dynamicProxy.TARGET_FOO);
	}
}