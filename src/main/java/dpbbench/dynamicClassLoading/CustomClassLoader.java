package dpbbench.dynamicClassLoading;

import dpbbench.util.Source;
import dpbbench.util.Utility;

/**
 * Uses a custom ClassLoader to load a class. The target class (Target) is not
 * set in the classPath. The class is first compiled then loaded at runtime.
 *
 * Source file can be found at
 * src/main/resources/dynamicClassLoading/Target.java
 * 
 * @author Li Sui
 */
public class CustomClassLoader extends ClassLoader {

	public static boolean TARGET = false;
	private static CustomClassLoader INSTANCE = null;

	// for each INSTANCE, fields are reset to default.
	public static CustomClassLoader getINSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new CustomClassLoader();
			TARGET = false;
		}
		return INSTANCE;
	}

	@Source
	public void source() throws Exception {
		CustomClassLoader classLoader = new CustomClassLoader();
		classLoader.loadClass("dynamicClassLoading.Target").newInstance();
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		byte[] content = new byte[0];
		try {
			content = Utility.compile(this.getClass().getClassLoader(), name);
		} catch (Exception e) {
		}
		return defineClass(name, content, 0, content.length);
	}
}
