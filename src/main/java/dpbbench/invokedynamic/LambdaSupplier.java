package dpbbench.invokedynamic;

import static dpbbench.util.Expected.NO;
import static dpbbench.util.Expected.YES;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Supplier interface represents an operation that accepts no arguments and
 * returns results. This scenario is modified to accept one arguments.
 * 
 * @require Java 8 and above
 * @author Li Sui
 */
public class LambdaSupplier {

	public boolean TARGET_STRING = false;
	public boolean TARGET_NO_PARAM = false;

	@Source
	public void source() {
		java.util.function.Supplier<String> i = () -> target("input");
		i.get();
	}

	@Target(expectation = NO)
	public String target() {
		this.TARGET_NO_PARAM = true;
		return null;
	}

	@Target(expectation = YES)
	public String target(String input) {
		this.TARGET_STRING = true;
		return null;
	};
}
