package dpbbench.util;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;

/**
 * Utility class
 * 
 * @author li Sui
 */
public class Utility {

	public static sun.misc.Unsafe getUnsafe() {
		sun.misc.Unsafe unsafe = null;
		try {
			Field f = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
			f.setAccessible(true);

			unsafe = (sun.misc.Unsafe) f.get(null);
		} catch (Exception e) {
		}
		return unsafe;
	}

	public static byte[] compile(ClassLoader classLoader, String name) throws Exception {
		String path = name.replace(".", "/");
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		compiler.run(null, null, null, classLoader.getResource(path + ".java").getFile());
		File file = new File(classLoader.getResource(path + ".class").getFile());
		FileInputStream input = new FileInputStream(file);
		byte[] content = new byte[(int) file.length()];
		input.read(content);
		input.close();
		return content;
	}

	public static byte[] serialise(Object obj) throws Exception {
		ByteArrayOutputStream ba = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(ba);
		oos.writeObject(obj);
		oos.close();
		return ba.toByteArray();
	}
}
