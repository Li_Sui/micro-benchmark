package dpbbench.reflection.instantiation;

import static dpbbench.util.Expected.YES;
import static dpbbench.util.Expected.NO;
import dpbbench.util.Source;

public class Intraprocedural1 {
	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() throws Exception {
		String className = new StringBuilder("tegraT").reverse().toString();
		Class foo = Class.forName("dpbbench.reflection.instantiation.Intraprocedural1$" + className);
		foo.getConstructor(Intraprocedural1.class).newInstance(this);
	}

	public class Target {
		@dpbbench.util.Target(expectation = YES)
		public Target() {
			TARGET = true;
		}
	}

	public class tegraT {
		@dpbbench.util.Target(expectation = NO)
		public tegraT() {
			TARGET2 = true;
		}
	}
}
