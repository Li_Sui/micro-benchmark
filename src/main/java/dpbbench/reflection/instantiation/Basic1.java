package dpbbench.reflection.instantiation;

import dpbbench.util.Expected;
import dpbbench.util.Source;

/**
 * Basic usage of dynamic class loading
 * @author Li Sui
 */
public class Basic1 {
    public boolean TARGET;
    public boolean TARGET2;

    @Source
    public void source() throws Exception{
        Class claz =Target.class;
        //An inner class has a "default constructor" which takes an instance of its outer class
        claz.getConstructor(Basic1.class).newInstance(this);
    }

    public class Target {
        @ dpbbench.util.Target(expectation= Expected.YES)
        public Target(){
            TARGET=true;
        }
    }

    public class Target2 {
        @ dpbbench.util.Target(expectation= Expected.NO)
        public Target2(){
            TARGET2=true;
        }
    }
}
