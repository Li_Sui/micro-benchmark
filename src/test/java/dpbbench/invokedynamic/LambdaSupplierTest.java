package dpbbench.invokedynamic;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.NegativeTests;
import dpbbench.PositiveTests;
import dpbbench.invokedynamic.LambdaSupplier;

/**
 * @author Li Sui
 */
public class LambdaSupplierTest {
	private LambdaSupplier lambda;

	@Before
	public void setUp() throws Exception {
		lambda = new LambdaSupplier();
		lambda.source();
	}

	@After
	public void tearDown() throws Exception {
		lambda = null;
	}

	/**
	 * Test if LambdaSupplier.source() does call LambdaSupplier.target(String);
	 */
	@Category(PositiveTests.class)
	@Test
	public void testMethodTargetWithParamString() {
		Assert.assertTrue(lambda.TARGET_STRING);
	}

	/**
	 * Test if LambdaSupplier.source() does not call LambdaSupplier.target();
	 */
	@Category(NegativeTests.class)
	@Test
	public void testMethodTargetThatHasNoParam() {
		Assert.assertFalse(lambda.TARGET_NO_PARAM);
	}
}