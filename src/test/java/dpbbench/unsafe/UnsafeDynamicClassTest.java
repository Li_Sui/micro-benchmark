package dpbbench.unsafe;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import dpbbench.PositiveTests;
import dpbbench.unsafe.UnsafeDynamicClass;


/**
 * @author Li Sui
 */
public class UnsafeDynamicClassTest {
	private UnsafeDynamicClass unsafeDynamicClass;

	@Before
	public void setUp() throws Exception {
		unsafeDynamicClass = UnsafeDynamicClass.getINSTANCE();
		unsafeDynamicClass.source();
	}

	@After
	public void tearDown() throws Exception {
		unsafeDynamicClass = null;
	}

	/**
	 * test if UnsafeDynamicClassTest.source() loads Target which is not in the
	 * classPath
	 */
	@Category(PositiveTests.class)
	@Test
	public void testLoadingClassTarget() {
		Assert.assertTrue(unsafeDynamicClass.TARGET);
	}
}