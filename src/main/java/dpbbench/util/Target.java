package dpbbench.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.METHOD,ElementType.CONSTRUCTOR})
public @interface Target {
    Expected expectation();
    Class<?> belongsTo() default Object.class;
}
