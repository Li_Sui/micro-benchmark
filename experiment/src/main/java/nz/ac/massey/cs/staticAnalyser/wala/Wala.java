package nz.ac.massey.cs.staticAnalyser.wala;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.*;
import com.ibm.wala.ipa.callgraph.impl.ArgumentTypeEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyFactory;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.Predicate;
import com.ibm.wala.util.collections.CollectionFilter;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.GraphSlicer;
import com.ibm.wala.util.io.FileProvider;
import probe.CallEdge;
import probe.ObjectManager;
import probe.ProbeMethod;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Wala call graph construction
 * @author li sui
 */
public class Wala {

    private probe.CallGraph probeCG;

    //put all jar in processDir
    public void run(String processDir,String exclusionsFilePath,boolean reflectionAnalysis) throws Exception{
        AnalysisScope scope = AnalysisScopeReader.makeJavaBinaryAnalysisScope(processDir, (new FileProvider()).getFile(exclusionsFilePath));

        ClassHierarchy cha = ClassHierarchyFactory.make(scope);

        //set all methods to be entry point
        final HashSet<Entrypoint> result = HashSetFactory.make();
        for (IClass klass : cha) {
            if (!klass.isInterface() && klass.getClassLoader().getReference().equals(ClassLoaderReference.Application)) {
                for(IMethod m : klass.getDeclaredMethods()) {
                    if (!m.isAbstract()){
                        result.add(new ArgumentTypeEntrypoint(m, cha));
                    }
                }
            }
        }

        Iterable<Entrypoint> entrypoints = new Iterable<Entrypoint>() {
            public Iterator<Entrypoint> iterator() {
                return result.iterator();
            }
        };

        AnalysisOptions options = new AnalysisOptions(scope, entrypoints);
        //configure reflection option here
        if(reflectionAnalysis) {
            options.setReflectionOptions(AnalysisOptions.ReflectionOptions.FULL);
        }else{
            options.setReflectionOptions(AnalysisOptions.ReflectionOptions.NONE);
        }
        //apply different analysis
        com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = Util.makeZeroCFABuilder(options, new AnalysisCacheImpl(), cha, scope);
        CallGraph allCG = builder.makeCallGraph(options, null);
        System.err.println(CallGraphStats.getStats(allCG));
        //filter call graph node
        //Graph<CGNode> applicationCG =pruneGraph(allCG, new ApplicationLoaderFilter());
        //build with probe
        probeCG=build(allCG);
    }

    private probe.CallGraph build(Graph<CGNode> walaCG ) throws Exception{
        probe.CallGraph cg=new probe.CallGraph();
        Iterator<CGNode> iter =walaCG.iterator();
        while(iter.hasNext()){
            CGNode n=iter.next();
            Iterator<CGNode> succIter=walaCG.iterator();
            while(succIter.hasNext()) {
                CGNode n2 = succIter.next();
                if (walaCG.hasEdge(n, n2)) {
                    ProbeMethod source =buildProbeMethod(n);
                    ProbeMethod target=buildProbeMethod(n2);
                    probe.CallEdge probeEdge = new CallEdge(source,target);
                    cg.entryPoints().add(source);
                    ((Set<CallEdge>)cg.edges()).add(probeEdge);
                }
            }
        }
        return cg;
    }

    private ProbeMethod buildProbeMethod(CGNode n){
        String declaredClass = stringify(n.getMethod().getDeclaringClass().getName().toString());

        String name = n.getMethod().getName().toString();

        String returnType =stringify(n.getMethod().getReturnType().getName().toString());
        StringBuilder paramType = new StringBuilder();

        for(int i =1; i<n.getMethod().getNumberOfParameters();i++){
            paramType.append(stringify(n.getMethod().getParameterType(i).getName().toString()));
            if(i<n.getMethod().getNumberOfParameters()-1){
                paramType.append(",");
            }
        }
        String signature=returnType+" "+name+"("+paramType.toString()+")";

        return ObjectManager.v().getMethod(ObjectManager.v()
                .getClass(declaredClass),name,signature);
    }
    private String stringify(String s){
        //object
        if(s.startsWith("L")){
            return s.substring(1).replace("/",".");
        }
        //void return type
        if(s.equals("V")){
            return "void";
        }
        //primitive
        if(s.equals("I")){
            return "int";
        }
        if(s.equals("B")){
            return "byte";
        }
        if(s.equals("J")){
            return "long";
        }
        if(s.equals("F")){
            return "float";
        }
        if(s.equals("D")){
            return "double";
        }
        if(s.equals("S")){
            return "short";
        }
        if(s.equals("C")){
            return "char";
        }
        if(s.equals("Z")){
            return "boolean";
        }

        //array
        if(s.startsWith("[L") || s.startsWith("[I") || s.startsWith("[B") || s.startsWith("[J")||
                s.startsWith("[F") ||
                s.startsWith("[D") ||
                s.startsWith("[S") ||
                s.startsWith("[C") ||
                s.startsWith("[Z")){
            return stringify(s.substring(1));
        }
        return s;
    }

    public static <T> Graph<T> pruneGraph(Graph<T> g, Predicate<T> f) throws WalaException {
        Collection<T> slice = GraphSlicer.slice(g, f);
        return GraphSlicer.prune(g, new CollectionFilter<>(slice));
    }

//    private static class ApplicationLoaderFilter extends Predicate<CGNode> {
//
//        @Override public boolean test(CGNode o) {
//            if (o instanceof CGNode) {
//                CGNode n = (CGNode) o;
//                return n.getMethod().getDeclaringClass().getClassLoader().getReference().equals(ClassLoaderReference.Application);
//            } else if (o instanceof LocalPointerKey) {
//                LocalPointerKey l = (LocalPointerKey) o;
//                return test(l.getNode());
//            } else {
//                return false;
//            }
//        }
//    }

    public probe.CallGraph getProbeCG(){
        return this.probeCG;
    }
}
