package nz.ac.massey.cs.staticAnalyser.util;

import probe.CallEdge;
import probe.CallGraph;

import java.util.ArrayList;
import java.util.List;

public class Result {
    private String analyser;

    private CallGraph analyserCG;

    private List<CallEdge> TP =new ArrayList<>();
    private List<CallEdge> FN=new ArrayList<>();
    private List<CallEdge> FP=new ArrayList<>();
    private List<CallEdge> WACC=new ArrayList<>();


    public Result(String analyser){
        this.analyser=analyser;
    }

    public void addFN(CallEdge edge) {
        this.FN.add(edge);
    }

    public void addFP(CallEdge edge) {
        this.FP.add(edge);
    }

    public void addWACC(CallEdge edge) {
        this.WACC.add(edge);
    }

    public String getAnalyser() {
        return analyser;
    }

    public List<CallEdge> getTP() { return TP; }

    public List<CallEdge> getFN() {
        return FN;
    }

    public List<CallEdge> getFP() {
        return FP;
    }

    public int getFNFP() {
        return FN.size()+FP.size();
    }

    public List<CallEdge> getWACC() {
        return WACC;
    }

    public void setAnalyserCG(CallGraph cg){
        this.analyserCG=cg;
    }
    public CallGraph getAnalyserCG() {
        return analyserCG;
    }
}
