package dpbbench.vanilla;

import static dpbbench.util.Expected.YES;
import static dpbbench.util.Expected.NO;
import dpbbench.util.Source;
import dpbbench.util.Target;

/**
 * Baseline: straight invocation of target() by source() with invokevirtual
 * instruction.
 * 
 * @author Li Sui
 */
public class Vanilla {

	public boolean TARGET = false;
	public boolean TARGET2 = false;

	@Source
	public void source() {
		target();
	}

	@Target(expectation = YES)
	public void target() {
		this.TARGET = true;
	}

	@Target(expectation = NO)
	public void target(int o) {
		this.TARGET2 = true;
	}
}
